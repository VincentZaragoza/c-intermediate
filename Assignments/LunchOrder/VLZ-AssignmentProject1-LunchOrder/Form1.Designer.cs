﻿namespace VLZ_AssignmentProject1_LunchOrder
{
    partial class frmLunchOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnPlaceOrder = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.grbMainCourse = new System.Windows.Forms.GroupBox();
            this.radSalad = new System.Windows.Forms.RadioButton();
            this.radPizza = new System.Windows.Forms.RadioButton();
            this.radHamburger = new System.Windows.Forms.RadioButton();
            this.grbAddOns = new System.Windows.Forms.GroupBox();
            this.chkThree = new System.Windows.Forms.CheckBox();
            this.chkTwo = new System.Windows.Forms.CheckBox();
            this.chkOne = new System.Windows.Forms.CheckBox();
            this.grbOrderTotal = new System.Windows.Forms.GroupBox();
            this.lblSubtotal = new System.Windows.Forms.Label();
            this.lblTax = new System.Windows.Forms.Label();
            this.lblOrderTotal = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.grbMainCourse.SuspendLayout();
            this.grbAddOns.SuspendLayout();
            this.grbOrderTotal.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnPlaceOrder
            // 
            this.btnPlaceOrder.Location = new System.Drawing.Point(343, 205);
            this.btnPlaceOrder.Name = "btnPlaceOrder";
            this.btnPlaceOrder.Size = new System.Drawing.Size(75, 23);
            this.btnPlaceOrder.TabIndex = 0;
            this.btnPlaceOrder.Text = "Place Order";
            this.btnPlaceOrder.UseVisualStyleBackColor = true;
            this.btnPlaceOrder.Click += new System.EventHandler(this.btnPlaceOrder_Click);
            // 
            // btnExit
            // 
            this.btnExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnExit.Location = new System.Drawing.Point(343, 265);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 23);
            this.btnExit.TabIndex = 0;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // grbMainCourse
            // 
            this.grbMainCourse.Controls.Add(this.radSalad);
            this.grbMainCourse.Controls.Add(this.radPizza);
            this.grbMainCourse.Controls.Add(this.radHamburger);
            this.grbMainCourse.Location = new System.Drawing.Point(32, 39);
            this.grbMainCourse.Name = "grbMainCourse";
            this.grbMainCourse.Size = new System.Drawing.Size(200, 100);
            this.grbMainCourse.TabIndex = 1;
            this.grbMainCourse.TabStop = false;
            this.grbMainCourse.Text = "Main Course";
            // 
            // radSalad
            // 
            this.radSalad.AutoSize = true;
            this.radSalad.Location = new System.Drawing.Point(11, 65);
            this.radSalad.Name = "radSalad";
            this.radSalad.Size = new System.Drawing.Size(88, 17);
            this.radSalad.TabIndex = 2;
            this.radSalad.Text = "Salad - $4.95";
            this.radSalad.UseVisualStyleBackColor = true;
            this.radSalad.CheckedChanged += new System.EventHandler(this.radSalad_CheckedChanged);
            // 
            // radPizza
            // 
            this.radPizza.AutoSize = true;
            this.radPizza.Location = new System.Drawing.Point(11, 39);
            this.radPizza.Name = "radPizza";
            this.radPizza.Size = new System.Drawing.Size(86, 17);
            this.radPizza.TabIndex = 1;
            this.radPizza.Text = "Pizza - $5.95";
            this.radPizza.UseVisualStyleBackColor = true;
            this.radPizza.CheckedChanged += new System.EventHandler(this.radPizza_CheckedChanged);
            // 
            // radHamburger
            // 
            this.radHamburger.AutoSize = true;
            this.radHamburger.Checked = true;
            this.radHamburger.Location = new System.Drawing.Point(11, 16);
            this.radHamburger.Name = "radHamburger";
            this.radHamburger.Size = new System.Drawing.Size(113, 17);
            this.radHamburger.TabIndex = 0;
            this.radHamburger.TabStop = true;
            this.radHamburger.Text = "Hamburger - $6.95";
            this.radHamburger.UseVisualStyleBackColor = true;
            this.radHamburger.CheckedChanged += new System.EventHandler(this.radHamburger_CheckedChanged);
            // 
            // grbAddOns
            // 
            this.grbAddOns.Controls.Add(this.chkThree);
            this.grbAddOns.Controls.Add(this.chkTwo);
            this.grbAddOns.Controls.Add(this.chkOne);
            this.grbAddOns.Location = new System.Drawing.Point(255, 39);
            this.grbAddOns.Name = "grbAddOns";
            this.grbAddOns.Size = new System.Drawing.Size(200, 100);
            this.grbAddOns.TabIndex = 0;
            this.grbAddOns.TabStop = false;
            this.grbAddOns.Text = "Add-on-Items ($.75/each)";
            // 
            // chkThree
            // 
            this.chkThree.AutoSize = true;
            this.chkThree.Location = new System.Drawing.Point(11, 65);
            this.chkThree.Name = "chkThree";
            this.chkThree.Size = new System.Drawing.Size(84, 17);
            this.chkThree.TabIndex = 2;
            this.chkThree.Text = "French Fries";
            this.chkThree.UseVisualStyleBackColor = true;
            this.chkThree.CheckedChanged += new System.EventHandler(this.chkThree_CheckedChanged);
            // 
            // chkTwo
            // 
            this.chkTwo.AutoSize = true;
            this.chkTwo.Location = new System.Drawing.Point(11, 42);
            this.chkTwo.Name = "chkTwo";
            this.chkTwo.Size = new System.Drawing.Size(142, 17);
            this.chkTwo.TabIndex = 1;
            this.chkTwo.Text = "Ketchup, Mustard, Mayo";
            this.chkTwo.UseVisualStyleBackColor = true;
            this.chkTwo.CheckedChanged += new System.EventHandler(this.chkTwo_CheckedChanged);
            // 
            // chkOne
            // 
            this.chkOne.AutoSize = true;
            this.chkOne.Location = new System.Drawing.Point(11, 19);
            this.chkOne.Name = "chkOne";
            this.chkOne.Size = new System.Drawing.Size(143, 17);
            this.chkOne.TabIndex = 0;
            this.chkOne.Text = "Lettuce, Tomato, Onions";
            this.chkOne.UseVisualStyleBackColor = true;
            this.chkOne.CheckedChanged += new System.EventHandler(this.chkOne_CheckedChanged);
            // 
            // grbOrderTotal
            // 
            this.grbOrderTotal.Controls.Add(this.lblSubtotal);
            this.grbOrderTotal.Controls.Add(this.lblTax);
            this.grbOrderTotal.Controls.Add(this.lblOrderTotal);
            this.grbOrderTotal.Controls.Add(this.label3);
            this.grbOrderTotal.Controls.Add(this.label2);
            this.grbOrderTotal.Controls.Add(this.label1);
            this.grbOrderTotal.Location = new System.Drawing.Point(32, 173);
            this.grbOrderTotal.Name = "grbOrderTotal";
            this.grbOrderTotal.Size = new System.Drawing.Size(239, 115);
            this.grbOrderTotal.TabIndex = 0;
            this.grbOrderTotal.TabStop = false;
            this.grbOrderTotal.Text = "Order Total";
            // 
            // lblSubtotal
            // 
            this.lblSubtotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblSubtotal.Location = new System.Drawing.Point(125, 16);
            this.lblSubtotal.Name = "lblSubtotal";
            this.lblSubtotal.Size = new System.Drawing.Size(100, 23);
            this.lblSubtotal.TabIndex = 5;
            // 
            // lblTax
            // 
            this.lblTax.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblTax.Location = new System.Drawing.Point(125, 49);
            this.lblTax.Name = "lblTax";
            this.lblTax.Size = new System.Drawing.Size(100, 23);
            this.lblTax.TabIndex = 4;
            // 
            // lblOrderTotal
            // 
            this.lblOrderTotal.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblOrderTotal.Location = new System.Drawing.Point(125, 82);
            this.lblOrderTotal.Name = "lblOrderTotal";
            this.lblOrderTotal.Size = new System.Drawing.Size(100, 23);
            this.lblOrderTotal.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Order Total:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(24, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tax (7.75%):";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Subtotal:";
            // 
            // frmLunchOrder
            // 
            this.AcceptButton = this.btnPlaceOrder;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnExit;
            this.ClientSize = new System.Drawing.Size(467, 348);
            this.Controls.Add(this.grbAddOns);
            this.Controls.Add(this.grbOrderTotal);
            this.Controls.Add(this.grbMainCourse);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnPlaceOrder);
            this.Name = "frmLunchOrder";
            this.Text = "Lunch Order";
            this.grbMainCourse.ResumeLayout(false);
            this.grbMainCourse.PerformLayout();
            this.grbAddOns.ResumeLayout(false);
            this.grbAddOns.PerformLayout();
            this.grbOrderTotal.ResumeLayout(false);
            this.grbOrderTotal.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnPlaceOrder;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.GroupBox grbMainCourse;
        private System.Windows.Forms.RadioButton radSalad;
        private System.Windows.Forms.RadioButton radPizza;
        private System.Windows.Forms.RadioButton radHamburger;
        private System.Windows.Forms.GroupBox grbAddOns;
        private System.Windows.Forms.CheckBox chkThree;
        private System.Windows.Forms.CheckBox chkTwo;
        private System.Windows.Forms.CheckBox chkOne;
        private System.Windows.Forms.GroupBox grbOrderTotal;
        private System.Windows.Forms.Label lblSubtotal;
        private System.Windows.Forms.Label lblTax;
        private System.Windows.Forms.Label lblOrderTotal;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}

