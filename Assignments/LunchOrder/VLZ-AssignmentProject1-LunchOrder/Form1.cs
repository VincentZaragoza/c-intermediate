﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VLZ_AssignmentProject1_LunchOrder
{
    public partial class frmLunchOrder : Form
    {

        public frmLunchOrder()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnPlaceOrder_Click(object sender, EventArgs e)
        {
            double subtotal = 0,tax;

            if (radHamburger.Checked)//Adds price of addons
            {        
                subtotal = subtotal + 6.95;

                if (chkOne.Checked)
                {
                    subtotal += .75;
                }
                if (chkTwo.Checked)
                {
                    subtotal += .75;
                }
                if (chkThree.Checked)
                {
                    subtotal += .75;
                }

            }

            if(radPizza.Checked)//Adds price of addons
            {
                subtotal = subtotal + 5.95;

                if (chkOne.Checked)
                {
                    subtotal += .50 ;
                }
                if (chkTwo.Checked)
                {
                    subtotal += .50;
                }
                if (chkThree.Checked)
                {
                    subtotal += .50;
                }
            }

            if (radSalad.Checked)//Adds price of addons
            {
                subtotal = subtotal + 4.95;

                if (chkOne.Checked)
                {
                    subtotal += .25;
                }
                if (chkTwo.Checked)
                {
                    subtotal += .25;
                }
                if (chkThree.Checked)
                {
                    subtotal += .25;
                }
            }
            
            //Calculating Tax
            tax = subtotal * .0775;

           //Displays totals
            lblSubtotal.Text = subtotal.ToString("c");
            lblTax.Text = tax.ToString("c");
            lblOrderTotal.Text = (tax + subtotal).ToString("c");



        }

        private void Clear()
        {
            //Clears labels
            lblOrderTotal.Text = string.Empty;
            lblSubtotal.Text = string.Empty;
            lblTax.Text = string.Empty;
        }

        private void ChangeAddon()
        {
            //Changes text for addon group box

            if (radHamburger.Checked)
            {
                grbAddOns.Text = "Add-on-Items ($.75/each)";
                chkOne.Text = "Lettuce, Tomato, Onions";
                chkTwo.Text = "Ketchup, Mustard, Mayo";
                chkThree.Text = "French Fries";
            }
            if (radPizza.Checked)
            {
                grbAddOns.Text = "Add-on-Items ($.50/each)";
                chkOne.Text = "Pepperoni";
                chkTwo.Text = "Sausage";
                chkThree.Text = "Olives";
            }
            if (radSalad.Checked)
            {
                grbAddOns.Text = "Add-on-Items ($.25/each)";
                chkOne.Text = "Croutons";
                chkTwo.Text = "Bacon bits";
                chkThree.Text = "Bread sticks";
            }
        }

        private void chkThree_CheckedChanged(object sender, EventArgs e)
        {
            Clear();
            ChangeAddon();
        }

        private void chkTwo_CheckedChanged(object sender, EventArgs e)
        {
            Clear();
        }

        private void chkOne_CheckedChanged(object sender, EventArgs e)
        {
            Clear();
        }

        private void radHamburger_CheckedChanged(object sender, EventArgs e)
        {
            Clear();
            ChangeAddon();
        }

        private void radPizza_CheckedChanged(object sender, EventArgs e)
        {
            Clear();
            ChangeAddon();
        }

        private void radSalad_CheckedChanged(object sender, EventArgs e)
        {
            Clear();
            ChangeAddon();
        }
    }
}
