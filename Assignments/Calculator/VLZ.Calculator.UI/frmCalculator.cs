﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VLZ.Calculator.BL;

namespace VLZ.Calculator
{
    public partial class frmCalculator : Form
    {
        BL.Calculator calculator = new BL.Calculator();
        public frmCalculator()
        {
            InitializeComponent();
        }

        private void btnEqual_Click(object sender, EventArgs e)
        {
            if (calculator.Operation != null)
            {
                calculator.SecondNumber = double.Parse(lblDisplay.Text);
                switch (calculator.Operation)
                {

                    case "+":
                        lblStatus.Text = "";
                        calculator.Result = calculator.Add(calculator.FirstNumber, calculator.SecondNumber);
                        lblDisplay.Text = calculator.Result.ToString();
                        calculator.Operation = null;
                        break;
                    case "-":
                        lblStatus.Text = "";
                        calculator.Result = calculator.Subtract(calculator.FirstNumber, calculator.SecondNumber);
                        lblDisplay.Text = calculator.Result.ToString();
                        calculator.Operation = null;
                        break;
                    case "*":
                        lblStatus.Text = "";
                        calculator.Result = calculator.Multiply(calculator.FirstNumber, calculator.SecondNumber);
                        lblDisplay.Text = calculator.Result.ToString();
                        calculator.Operation = null;
                        break;
                    case "/":

                        if(calculator.FirstNumber == 0 && calculator.SecondNumber == 0)
                        {
                            lblStatus.ForeColor = Color.Red;
                            lblStatus.Text = "Result is undefined";
                            calculator.Operation = null;
                        }else
                        {
                            calculator.Result = calculator.Divide(calculator.FirstNumber, calculator.SecondNumber);
                            lblDisplay.Text = calculator.Result.ToString();
                            calculator.Operation = null;
                        }
                        break;

                }

            }
        }
        
        private void button_click(object sender, EventArgs e)
        {
            Button button = (Button)sender;//Creates button object from sender(button pressed) to get text from
            string output = lblDisplay.Text;
            lblStatus.Text = "";

            calculator.Display(button.Text, ref output);//Calls Display function and modifies output text

            lblDisplay.Text = output;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            //Clears Text
            lblDisplay.Text = "0";
            calculator.Operation = null;
            calculator.FirstNumber = 0;
            calculator.SecondNumber = 0;
            lblStatus.Text = "";
        }

        private void operation_click(object sender, EventArgs e)
        {
            Button button = (Button)sender;//Creates button object from sender(button pressed) to get text from

            if (calculator.Operation == null)
            {
                //Sets First Number and operation
                calculator.FirstNumber = double.Parse(lblDisplay.Text);
                calculator.Operation = button.Text;
                //Clears Text
                lblDisplay.Text = "0";
                lblStatus.Text = "";
            }
            else
            {
                //Displays error message
                lblStatus.ForeColor = Color.Red;
                lblStatus.Text = "Invaid input";

            }
        }

        

        private void btnSign_Click(object sender, EventArgs e)
        {
            if (calculator.Operation != null)
            {
                lblStatus.Text = "";
                calculator.SecondNumber = double.Parse(lblDisplay.Text);
                calculator.SecondNumber = calculator.SecondNumber * -1;
                lblDisplay.Text = calculator.SecondNumber.ToString();
            }
            else
            {
                lblStatus.Text = "";
                calculator.FirstNumber = double.Parse(lblDisplay.Text);
                calculator.FirstNumber = calculator.FirstNumber * -1;
                lblDisplay.Text = calculator.FirstNumber.ToString();
            }
        }

        private void btnBack_Click(object sender, EventArgs e)
        {
            int index = lblDisplay.Text.Length;

            if(index==1)
            {
                lblDisplay.Text = "0";
            }else 
            {
                lblDisplay.Text = lblDisplay.Text.Substring(0, index - 1);                
            }
        }

        private void btnSqrt_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            double number=0;
            number = double.Parse(lblDisplay.Text);

            if(number > 0)
            {
                number = Math.Sqrt(number);
                lblDisplay.Text = number.ToString();
            }else
            {
                lblStatus.ForeColor = Color.IndianRed;
                lblStatus.Text = "Invalid input";
            }
        }

        private void btnReciprocal_Click(object sender, EventArgs e)
        {
            double number = 0;

            number = double.Parse(lblDisplay.Text);

            if (number == 0)
            {
                lblStatus.ForeColor = Color.Red;
                lblStatus.Text = "Result is undefined";
            }
            else
            {
                number = 1 / number;
                lblDisplay.Text = number.ToString();
            }

        }
    }
}
