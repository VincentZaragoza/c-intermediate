﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VLZ.Calculator.BL
{
    public class Calculator
    {
        private double firstNumber;
        public double FirstNumber
        {
            get { return firstNumber; }
            set { firstNumber = value; }
        }

        private double secondNumber;
        public double SecondNumber
        {
            get { return secondNumber; }
            set { secondNumber = value; }
        }

        private double result;

        public double Result
        {
            get { return result; }
            set { result = value; }
        }


        private string operation;
        public string Operation
        {
            get { return operation; }
            set { operation = value; }
        }

        public double Add(double x,double y)
        {
            double sum = 0;

            sum = x + y;

            return sum;
        }

        public double Subtract(double x, double y)
        {
            double difference = 0;

            difference = x - y;

            return difference;
        }
        public double Multiply(double x, double y)
        {
            double product = 0;

            product = x * y;

            return product;
        }

        public double Divide(double x, double y)
        {
            double product = 0;

            product = x / y;

            return product;
        }

        public void Display(string input, ref string output)
        {
            if(output == "0")
            {
                output = input;
            }
            else
            {
                output = output + input;
            }
        }
        

        public Calculator()
        {

        }

    }
}
