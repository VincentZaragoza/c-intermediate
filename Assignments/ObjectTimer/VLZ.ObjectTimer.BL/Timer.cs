﻿using System;

namespace VLZ.ObjectTimer.BL
{
    public class Timer
    {
        private bool IsRunning { get; set; }

        private DateTime _startTime;
        public DateTime StartTime
        {
            get { return _startTime; }
        }

        private DateTime _stoptime;
        public DateTime StopTime
        {
            get { return _stoptime; }
        }

        public string ElapsedTime
        {
            get
            {
                TimeSpan span = ETime;
                if (IsRunning)
                {
                    span += DateTime.Now - StartTime;
                }
                return span.ToString(@"hh\:mm\:ss");
            }
        }

        public TimeSpan ETime { get; set; }

        public Timer()
        {
            ETime = new TimeSpan();
        }

        public void StartClock()
        {
            IsRunning = true;
            _startTime = DateTime.Now;
        }

        public void StopClock()
        {                        
            IsRunning = false;
            _stoptime = DateTime.Now;
            ETime += DateTime.Now - StartTime;
        }

        public bool TestMethod()
        {
            return IsRunning;
        }
    }
}
