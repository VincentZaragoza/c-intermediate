﻿using System;

namespace VLZ.ObjectTimer.BL
{
    public class StopException : Exception {
        public StopException() : base("Timer already off.")
        {

        }

        public StopException(string message) : base(message)
        {

        }
    }
}

