﻿using System;

namespace VLZ.ObjectTimer.BL
{
    public class StartException : Exception {
        public StartException() : base("Timer already on.")
        {

        }

        public StartException(string message) : base(message)
        {

        }
    }

}
