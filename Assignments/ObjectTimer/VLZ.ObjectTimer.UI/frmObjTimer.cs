﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using VLZ.ObjectTimer.BL;

namespace VLZ.ObjectTimer.UI
{
    public partial class frmObjTimer : Form
    {
        public BL.Timer Timer { get; }

        private Thread _updateThread;

        public frmObjTimer()
        {
            InitializeComponent();
            Timer = new BL.Timer();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            if (!Timer.TestMethod())
            {
                Timer.StartClock();
                ThreadStart start = new ThreadStart(UpdateText);
                _updateThread = new Thread(start);
                _updateThread.Start();
            }
            else
            {
                StartException ex = new StartException("Timer already on.");
                lblStatus.Text = ex.Message;
            }
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "";
            if (Timer.TestMethod())
            {
                Timer.StopClock();
                _updateThread.Abort();
            }
            else
            {
                StopException ex = new StopException("Timer already off.");
                lblStatus.Text = ex.Message;
            }
        }

        private void UpdateText()
        {
            while (Timer.TestMethod())
            {
                lblDisplay.Invoke((MethodInvoker) delegate {
                    lblDisplay.Text = Timer.ElapsedTime;
                });
            }
        }

        private void frmObjTimer_FormClosing(object sender, FormClosingEventArgs e)
        {
            _updateThread.Abort();
        }
    }
}