﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VLZ.FileIO.UI
{
    public partial class frmFileIO : Form
    {
        private string filename = string.Empty;

        public frmFileIO()
        {
            InitializeComponent();
        }

        private void mnuFileOpen_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                StreamReader reader;

                ofd.Title = "Please pick a file";
                ofd.InitialDirectory = @"c:\Users\Public";
                ofd.Filter = "All Files (*.*)|*.*|Text Files (*.txt)|*.txt";

                if(ofd.ShowDialog() == DialogResult.OK  ) //User picks filename
                {
                    filename = ofd.FileName;
                    reader = File.OpenText(ofd.FileName);
                    txtInput.Text = reader.ReadToEnd();

                    lblStatus.Text = "File Opened: " + ofd.FileName;
                    lblStatus.ForeColor = Color.Blue;
                    reader.Close();
                }
                else
                {
                    lblStatus.Text = "No file selected...";
                    lblStatus.ForeColor = Color.Blue;
                }
                reader = null;
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.Red;
            } 
        }

        private void mnuFileNew_Click(object sender, EventArgs e)
        {
            StreamWriter writer;
            if(txtInput.Text == string.Empty)
            {
                txtInput.Text = string.Empty;
                filename = string.Empty;
                lblStatus.Text = string.Empty;
               
            }
            else
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Title = "Pick a file to save to";
                sfd.InitialDirectory = @"c:\Users\Public";

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    filename = sfd.FileName;

                    writer = File.CreateText(filename);
                    writer.WriteLine(txtInput.Text);

                    lblStatus.Text = "File Saved: " + filename;
                    lblStatus.ForeColor = Color.Blue;
                    lblStatus.Text = "File Saved: " + filename;

                    txtInput.Text = string.Empty;
                    writer.Close();
                    writer = null;
                }
            }


        }

        private void mnuFileSave_Click(object sender, EventArgs e)
        {
            try
            {
                StreamWriter writer;

                if(filename != string.Empty)
                {
                    writer = File.CreateText(filename);
                    writer.WriteLine(txtInput.Text);

                    writer.Close();
                    writer = null;

                    lblStatus.Text = "File Saved: " + filename;
                    lblStatus.ForeColor = Color.Blue;
                }
                else
                {
                    SaveFileDialog sfd = new SaveFileDialog();
                    sfd.Title = "Pick a file to save as";
                    sfd.InitialDirectory = @"c:\Users\Public";

                    if (sfd.ShowDialog() == DialogResult.OK)
                    {
                        filename = sfd.FileName;
                        writer = File.CreateText(filename);
                        writer.WriteLine(txtInput.Text);


                        writer.Close();
                        writer = null;

                        lblStatus.Text = "File Saved: " + filename;
                        lblStatus.ForeColor = Color.Blue;
                    }
                }
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.Red;
            }
        }

        private void mnuFileSaveAs_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfd = new SaveFileDialog();
                StreamWriter writer;

                sfd.Title = "Pick a file to save as";
                sfd.InitialDirectory = @"c:\Users\Public";

                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    filename = sfd.FileName;
                    writer = File.CreateText(filename);
                    writer.WriteLine(txtInput.Text);

                    writer.Close();
                    writer = null;

                    lblStatus.Text = "File Created: " + filename;
                    lblStatus.ForeColor = Color.Blue;
                }
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.Red;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblStatusTime.Text = DateTime.Now.ToLongTimeString();
        }
    }
}
