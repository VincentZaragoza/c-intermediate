﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VLZ.Banking.BL;

namespace VLZ.Banking.UI
{
    public partial class frmCustInfo : Form
    {
        Customers customers;
        public frmCustInfo()
        {
            InitializeComponent();
        }

        private void frmCustInfo_Load(object sender, EventArgs e)
        {
            try
            {
                customers = new Customers();
                customers.Populate();

                lbxCustomers.DataSource = customers;
                lbxCustomers.ValueMember = "CustromerId";
                lbxCustomers.DisplayMember = "FullName";
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.Red;

            }
        }

        private void lbxCustomers_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvWithDrawals.DataSource = null;
            dgvDeposits.DataSource = null;
            Customer pickedCustomer = customers[lbxCustomers.SelectedIndex];
            
            txtFName.Text = pickedCustomer.FirstName;
            txtLName.Text = pickedCustomer.LastName;
            txtSSN.Text = pickedCustomer.SSN.ToString();
            txtID.Text = pickedCustomer.CustromerId.ToString();
            txtBDay.Text = pickedCustomer.BirthDate.ToShortDateString();
                     
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnWithDrawals_Click(object sender, EventArgs e)
        {
            Customer pickedCustomer = customers[lbxCustomers.SelectedIndex];
            dgvWithDrawals.DataSource = pickedCustomer.WithDrawals;
        }

        private void btnDeposits_Click(object sender, EventArgs e)
        {
            Customer pickedCustomer = customers[lbxCustomers.SelectedIndex];
             dgvDeposits.DataSource = pickedCustomer.Deposits;
        }
    }
}
