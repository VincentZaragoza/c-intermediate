﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VLZ.Banking.BL
{
    public class Deposit
    {
        public int DepositID { get; set; }
        public double DepositAmount { get; set; }
        public DateTime DepositDate { get; set; }
    }
}
