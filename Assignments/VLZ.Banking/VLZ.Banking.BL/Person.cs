﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VLZ.Banking.BL
{
    public class Person
    {
        public string SSN { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int age { get; set; }
        public DateTime BirthDate { get; set; }
        public string FullName{
            get { return FirstName +" " + LastName; }           
        }

    }
}
