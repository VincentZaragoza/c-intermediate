﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VLZ.Banking.BL
{
    public class WithDrawal
    {
        public int WithDrawalId { get; set; }
        public double WithDrawalAmount { get; set; }
        public DateTime WithDrawalDate { get; set; }
    }
}
