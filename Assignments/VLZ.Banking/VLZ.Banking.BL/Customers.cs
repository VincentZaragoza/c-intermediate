﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VLZ.Banking.BL
{
    public class Customers : List<Customer>
    {
        public void Populate()
        {
           
            //Customer 1
            Customer customer1 = new Customer();
            customer1.Populate();
            this.Add(customer1);

            //Customer 2
            Customer customer2 = new Customer();
            customer2.CustromerId = Count + 1 ;
            customer2.SSN = "3413-21-1234";
            customer2.BirthDate = new DateTime(1978, 2, 14);
            customer2.FirstName = "Joe";
            customer2.LastName = "Smith";
            customer2.Populate(customer2);
            this.Add(customer2);

            //Customer 3
            Customer customer3 = new Customer();
            customer3.CustromerId = Count + 1;
            customer3.SSN = "6544-89-5641";
            customer3.BirthDate = new DateTime(1999, 12, 21);
            customer3.FirstName = "Vincent";
            customer3.LastName = "Zaragoza";

            Deposit deposit = new Deposit();
            deposit.DepositID = 1;
            deposit.DepositAmount = 799;
            deposit.DepositDate = new DateTime(2019, 4, 22);
            customer3.Deposits = new DepositList();
            customer3.Deposits.Add(deposit);

            WithDrawal withDrawal = new WithDrawal();
            withDrawal.WithDrawalId = 1;
            withDrawal.WithDrawalAmount = 399;
            withDrawal.WithDrawalDate = new DateTime(2019, 3, 21);
            customer3.WithDrawals = new WithDrawalList();
            customer3.WithDrawals.Add(withDrawal);
            this.Add(customer3);


        }
    }
}
