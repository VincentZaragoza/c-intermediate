﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VLZ.Banking.BL
{
    public class Customer : Person
    {
        public int CustromerId { get; set; }
        public DepositList Deposits{ get; set; }
        public WithDrawalList WithDrawals { get; set; }

        public void Populate()
        {
            //Create Customer
            Customer customer1 = new Customer();
            this.CustromerId = 1;
            this.BirthDate = new DateTime(1955, 10, 28);
            this.SSN = "1234-67-8788";
            this.FirstName = "Billy";
            this.LastName = "Gates";

            //Create Lists
            this.Deposits = new DepositList();
            Deposit deposit = new Deposit();

            //Add Deposit to List
            deposit.DepositID = Deposits.Count + 1;
            deposit.DepositAmount = 100000;
            deposit.DepositDate = new DateTime(2016, 7, 15);
            this.Deposits.Add(deposit);

            //Add Deposit to List
            deposit = new Deposit();
            deposit.DepositID = Deposits.Count + 1;
            deposit.DepositAmount = 5000000;
            deposit.DepositDate = new DateTime(2016, 10, 11);
            this.Deposits.Add(deposit);

            //Create Lists
            this.WithDrawals = new WithDrawalList();
            WithDrawal withDrawal = new WithDrawal();

            //Add WithDrawal to List
            withDrawal.WithDrawalId = WithDrawals.Count + 1;
            withDrawal.WithDrawalAmount = 20000;
            withDrawal.WithDrawalDate = new DateTime(2016, 8, 1);
            this.WithDrawals.Add(withDrawal);

            //Add WithDrawal to List
            withDrawal = new WithDrawal();
            withDrawal.WithDrawalId = WithDrawals.Count + 1;
            withDrawal.WithDrawalAmount = 30000;
            withDrawal.WithDrawalDate = new DateTime(2016, 12, 12);
            this.WithDrawals.Add(withDrawal);
        }

        public void Populate(Customer cust)
        {

            //Create Lists
            cust.Deposits = new DepositList();
            Deposit deposit = new Deposit();

            //Add Deposit to List
            deposit.DepositID = Deposits.Count + 1;
            deposit.DepositAmount = 1200;
            deposit.DepositDate = new DateTime(2018, 2, 14);
            cust.Deposits.Add(deposit);

            //Add Deposit to List
            deposit = new Deposit();
            deposit.DepositID = Deposits.Count + 1;
            deposit.DepositAmount = 1000;
            deposit.DepositDate = new DateTime(2018, 5, 23);
            cust.Deposits.Add(deposit);

            //Create Lists
            cust.WithDrawals = new WithDrawalList();
            WithDrawal withDrawal = new WithDrawal();

            //Add WithDrawal to List
            withDrawal.WithDrawalId = WithDrawals.Count + 1;
            withDrawal.WithDrawalAmount = 75;
            withDrawal.WithDrawalDate = new DateTime(2018, 7, 13);
            cust.WithDrawals.Add(withDrawal);

            //Add WithDrawal to List
            withDrawal = new WithDrawal();
            withDrawal.WithDrawalId = WithDrawals.Count + 1;
            withDrawal.WithDrawalAmount = 800;
            withDrawal.WithDrawalDate = new DateTime(2018, 4, 8);
            cust.WithDrawals.Add(withDrawal);
        }
    }
}
