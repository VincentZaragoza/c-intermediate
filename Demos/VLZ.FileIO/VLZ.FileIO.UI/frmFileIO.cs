﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace VLZ.FileIO.UI
{
    public partial class frmFileIO : Form
    {
        private string filename = "c:\\Users\\public\\data.txt";

        public frmFileIO()
        {
            InitializeComponent();
        }

        private void mnuExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void mnuWrite_Click(object sender, EventArgs e)
        {
            try
            {                
                StreamWriter sw;
                //Hook the write to the file. Pen/Paper
                sw = File.CreateText(filename); 

                //Write to the file
                sw.WriteLine(txtInput.Text);

                //Always close the file
                sw.Close();
                sw = null;

                lblStatus.Text = "File Written: " + filename;
                lblStatus.ForeColor = Color.Blue;
            }
            catch (Exception ex)
            {
                lblStatus.ForeColor = Color.Red;
                lblStatus.Text = ex.Message;
            }
        }

        private void mnuReadAll_Click(object sender, EventArgs e)
        {
            try
            {                
                StreamReader sr;

                sr = File.OpenText(filename);

                lblOutput.Text = sr.ReadToEnd();
                sr.Close();
                sr = null;

                lblStatus.ForeColor = Color.Blue;
                lblStatus.Text = "File Opened: " + filename;
            }
            catch (Exception ex)
            {
                lblStatus.ForeColor = Color.Red;
                lblStatus.Text = ex.Message;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if(filename != null && filename != string.Empty)
                {
                    if (File.Exists(filename))
                    {
                        File.Delete(filename);
                        lblStatus.ForeColor = Color.Blue;
                        lblStatus.Text = filename + " deleted...";
                    }
                    else
                    {
                        //throw new Exception("File does not exist. " + filename);

                        //OR

                        throw new FileNotFoundException("File does not exist. " + filename);
                    }
                }
                else
                {
                    throw new Exception("Filename is not set");
                }
            }
            catch(FileNotFoundException ex)
            {
                lblStatus.ForeColor = Color.Green;
                lblStatus.Text = ex.Message;
            }
            catch (Exception ex)
            {
                lblStatus.ForeColor = Color.Red;
                lblStatus.Text = ex.Message;
            }
            finally
            {
                lblStatus.Text += "finished...";
            }
        }
    }
}
