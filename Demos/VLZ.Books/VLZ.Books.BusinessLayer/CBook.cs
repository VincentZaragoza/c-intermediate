﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VLZ.Books.BusinessLayer
{
    // House
    public class CBook
    {
        public enum MediaType
        {
            Audio = 0,
            Digital = 1,
            Classical = 2
        }

        // Private field-TV
        private int myVar;

        // Public property-Window
        public int MyProperty
        {
            get { return myVar; } // Window (open or closed)
            set { myVar = value; }// Window (open)
        }


        private string title;

        public string Title
        {
            get { return title; }
            set { string title = value; }
        }

        private int numPages;

        public int NumPages
        {
            get { return numPages; }
            set { numPages = value; }
        }

        private MediaType media;

        public MediaType Media
        {
            get { return media; }
            set { media = value; }
        }

        private string author;

        public string Author
        {
            get { return author; }
            set { author = value; }
        }

        public void TellStory()
        {
            switch (media)
            {
                case MediaType.Audio:
                    //Play audio stream
                    break;
                case MediaType.Digital:
                    //Show the digital screen
                    break;
                case MediaType.Classical:
                    //Read a book
                    break;
            }
        }
    }
}
