﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDF.Books.BusinessLayer
{
    public enum MediaType
    {
        Audio = 0,
        Digital = 1,
        Classical = 2
    }

    // House
    public class CBook 
    {
        // Private field  - TV
        private int myVar;


        // Public property
        public int MyProperty
        {
            get { return myVar; }  // Window (open or closed)
            set { myVar = value; } // Window 
        }


        private string title;

        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        private int numpages;

        public int NumPages
        {
            get { return numpages; }
            set { numpages = value; }
        }

        private MediaType media;

        public MediaType Media
        {
            get { return media; }
            set { media = value; }
        }

        private string author;

        public string Author
        {
            get { return author; }
            set { author = value; }
        }

        private string isbn;

        public string ISBN
        {
            get { return isbn; }
            set { isbn = value; }
        }



        public string DisplayField
        {
            get { return this.Title + " - " + this.Author; }
        }


        public string TellStory()
        {
            string msg;

            switch (media)
            {
                case MediaType.Audio:
                    // Play the audio stream
                    msg = "Play the audio stream";
                    break;
                case MediaType.Digital:
                    // Show the digital thing
                    msg = "Show the digital thing";
                    break;
                case MediaType.Classical:
                    // Read a book
                    msg = "Read a book";
                    break;
                default:
                    msg = "None";
                    break;
            }
            return msg;
        }
    }
}
