﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDF.Books.BusinessLayer
{
    public class CBookList : List<CBook>
    {
        public void Load()
        {
            CBook book = new CBook();

            //set the properties

            book.Author = "Brian Foote";
            book.Title = "C# Programming";
            book.NumPages = 345;
            book.ISBN = "98415610565";
            book.Media = MediaType.Classical;

            //Add the book to the list
            Add(book);

            CBook book1 = new CBook();

            //set the properties            
            book.Author = "James Guy";
            book.Title = "Lord of the guys";
            book.NumPages = 600;
            book.ISBN = "98415567325";
            book.Media = MediaType.Classical;

            //Add the book to the list
            Add(book1);
        }
    }
}
