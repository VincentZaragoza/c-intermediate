﻿namespace BDF.Books.UI
{
    partial class frmBooks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGenerate = new System.Windows.Forms.Button();
            this.lbxBooks = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTitle = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtAuthor = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.nudNumberOfPages = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.cbxMedia = new System.Windows.Forms.ComboBox();
            this.btnMakeNew = new System.Windows.Forms.Button();
            this.btnTellStory = new System.Windows.Forms.Button();
            this.btnMakeBooks = new System.Windows.Forms.Button();
            this.cbxBooks = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudNumberOfPages)).BeginInit();
            this.SuspendLayout();
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(39, 324);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(91, 23);
            this.btnGenerate.TabIndex = 8;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // lbxBooks
            // 
            this.lbxBooks.FormattingEnabled = true;
            this.lbxBooks.Location = new System.Drawing.Point(308, 44);
            this.lbxBooks.Name = "lbxBooks";
            this.lbxBooks.Size = new System.Drawing.Size(420, 303);
            this.lbxBooks.TabIndex = 10;
            this.lbxBooks.SelectedIndexChanged += new System.EventHandler(this.lbxBooks_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "&Title:";
            // 
            // txtTitle
            // 
            this.txtTitle.Location = new System.Drawing.Point(39, 72);
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(237, 20);
            this.txtTitle.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "&Author:";
            // 
            // txtAuthor
            // 
            this.txtAuthor.Location = new System.Drawing.Point(39, 133);
            this.txtAuthor.Name = "txtAuthor";
            this.txtAuthor.Size = new System.Drawing.Size(237, 20);
            this.txtAuthor.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "&Number of Pages:";
            // 
            // nudNumberOfPages
            // 
            this.nudNumberOfPages.Location = new System.Drawing.Point(39, 203);
            this.nudNumberOfPages.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.nudNumberOfPages.Name = "nudNumberOfPages";
            this.nudNumberOfPages.Size = new System.Drawing.Size(237, 20);
            this.nudNumberOfPages.TabIndex = 5;
            this.nudNumberOfPages.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 249);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "&Media:";
            // 
            // cbxMedia
            // 
            this.cbxMedia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxMedia.FormattingEnabled = true;
            this.cbxMedia.Location = new System.Drawing.Point(39, 274);
            this.cbxMedia.Name = "cbxMedia";
            this.cbxMedia.Size = new System.Drawing.Size(237, 21);
            this.cbxMedia.TabIndex = 7;
            // 
            // btnMakeNew
            // 
            this.btnMakeNew.Location = new System.Drawing.Point(136, 324);
            this.btnMakeNew.Name = "btnMakeNew";
            this.btnMakeNew.Size = new System.Drawing.Size(91, 23);
            this.btnMakeNew.TabIndex = 9;
            this.btnMakeNew.Text = "Make New";
            this.btnMakeNew.UseVisualStyleBackColor = true;
            this.btnMakeNew.Click += new System.EventHandler(this.btnMakeNew_Click);
            // 
            // btnTellStory
            // 
            this.btnTellStory.Location = new System.Drawing.Point(39, 353);
            this.btnTellStory.Name = "btnTellStory";
            this.btnTellStory.Size = new System.Drawing.Size(91, 23);
            this.btnTellStory.TabIndex = 11;
            this.btnTellStory.Text = "Tell Story";
            this.btnTellStory.UseVisualStyleBackColor = true;
            this.btnTellStory.Click += new System.EventHandler(this.btnTellStory_Click);
            // 
            // btnMakeBooks
            // 
            this.btnMakeBooks.Location = new System.Drawing.Point(152, 353);
            this.btnMakeBooks.Name = "btnMakeBooks";
            this.btnMakeBooks.Size = new System.Drawing.Size(75, 23);
            this.btnMakeBooks.TabIndex = 12;
            this.btnMakeBooks.Text = "button1";
            this.btnMakeBooks.UseVisualStyleBackColor = true;
            this.btnMakeBooks.Click += new System.EventHandler(this.btnMakeBooks_Click);
            // 
            // cbxBooks
            // 
            this.cbxBooks.FormattingEnabled = true;
            this.cbxBooks.Location = new System.Drawing.Point(417, 353);
            this.cbxBooks.Name = "cbxBooks";
            this.cbxBooks.Size = new System.Drawing.Size(261, 21);
            this.cbxBooks.TabIndex = 13;
            // 
            // frmBooks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.cbxBooks);
            this.Controls.Add(this.btnMakeBooks);
            this.Controls.Add(this.btnTellStory);
            this.Controls.Add(this.btnMakeNew);
            this.Controls.Add(this.cbxMedia);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.nudNumberOfPages);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtAuthor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTitle);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbxBooks);
            this.Controls.Add(this.btnGenerate);
            this.Name = "frmBooks";
            this.Text = "Books";
            this.Load += new System.EventHandler(this.frmBooks_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nudNumberOfPages)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.ListBox lbxBooks;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTitle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtAuthor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nudNumberOfPages;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cbxMedia;
        private System.Windows.Forms.Button btnMakeNew;
        private System.Windows.Forms.Button btnTellStory;
        private System.Windows.Forms.Button btnMakeBooks;
        private System.Windows.Forms.ComboBox cbxBooks;
    }
}

