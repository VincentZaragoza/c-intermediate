﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BDF.Books.BusinessLayer;

namespace BDF.Books.UI
{
    public partial class frmBooks : Form
    {
        CBook myBook;
        CBookList bookList;

        public frmBooks()
        {
            InitializeComponent();
        }

        private void frmBooks_Load(object sender, EventArgs e)
        {
            //var medias = Enum.GetNames(typeof(MediaType));

            foreach(var media in Enum.GetNames(typeof(MediaType)))
            {
                cbxMedia.Items.Add(media);
            }
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            // Declare the Book variable
            CBook book;

            // Instantiate the book object.
            book = new CBook();

            // Set the values on the object.
            book.Title = "Humpty Dumpty";
            book.Author = "Lewis Caroll";
            book.NumPages = 345;
            book.Media = MediaType.Classical;

            lbxBooks.Items.Add(book.DisplayField);

            txtTitle.Text = book.Title;
            txtAuthor.Text = book.Author;
            nudNumberOfPages.Value = book.NumPages;
            cbxMedia.SelectedIndex = (int)book.Media;
        }

        private void btnMakeNew_Click(object sender, EventArgs e)
        {
            // Declare and instantiation of the object.
            CBook book = new CBook();

            book.Title = txtTitle.Text;
            book.Author = txtAuthor.Text;
            // Cast the decimal number to int.
            book.NumPages = (int)nudNumberOfPages.Value;
            book.Media = (MediaType)cbxMedia.SelectedIndex;

            lbxBooks.Items.Add(book.DisplayField);


        }

        private void btnTellStory_Click(object sender, EventArgs e)
        {
            try
            {
                myBook = new CBook();
                lbxBooks.Items.Add(myBook.TellStory());
            }
            catch (Exception ex)
            {

            }
        }

        private void btnMakeBooks_Click(object sender, EventArgs e)
        {
            bookList = new CBookList();
            bookList.Load();

            //Bind the booklst to the listbox
            lbxBooks.DataSource = bookList;
            lbxBooks.DisplayMember = "Title";
            lbxBooks.ValueMember = "ISBN";


            cbxBooks.DataSource = bookList;
            cbxBooks.DisplayMember = "Title";
            cbxBooks.ValueMember = "ISBN";
        }


        private void lbxBooks_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Get the object that i picked
            CBook book = bookList[lbxBooks.SelectedIndex];

            if(book != null)
            {
                txtAuthor.Text = book.Author;
                txtTitle.Text = book.Title;
                nudNumberOfPages.Value = book.NumPages;
                cbxMedia.SelectedIndex = (int)book.Media;
            }

        }
    }
}
