﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VLZ.Circle.UI
{
    public partial class frmCircle : Form
    {

        private double CalculateArea(double radius)
        {
            double area = 0;

            area = Math.PI * radius * radius;

            return area;

        }

        private void CalculateCircumference(double radius,out double circumference)
        {
            circumference = Math.PI * 2 * radius;
        }
        public frmCircle()
        {
            InitializeComponent();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            double radius = 0;

            try
            {
                if(double.TryParse(txtRadius.Text, out radius))
                {
                    double circumference = 0;

                    lblArea.Text = CalculateArea(radius).ToString("n2");

                    CalculateCircumference(radius, out circumference);
                    lblCircumference.Text = circumference.ToString("n2");

                }
                else
                {
                    lblStatus.Text = "Please enter a number";
                    lblStatus.ForeColor = Color.Red;
                    txtRadius.Focus();
                    txtRadius.SelectAll();
                }
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.Red;
                txtRadius.Focus();
                txtRadius.SelectAll();
            }
        }

        private void txtRadius_TextChanged(object sender, EventArgs e)
        {
            lblCircumference.Text = string.Empty;
            lblArea.Text = string.Empty;
        }
    }
}
