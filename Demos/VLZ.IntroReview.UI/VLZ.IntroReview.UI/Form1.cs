﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VLZ.IntroReview.UI
{
    public partial class frmIntroReview : Form
    {
        public frmIntroReview()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtInput.Clear();
            lblResult.Text = string.Empty;
        }

        private void txtInput_TextChanged(object sender, EventArgs e)
        {
            lblResult.Text = string.Empty;
            lblStatus.Text = string.Empty;
        }

        private double CalculateSquare(string input)
        {
            try
            {
                double dblInput;

                if (double.TryParse(input, out dblInput))
                {
                    //Good input
                    input = "4";
                    return dblInput * dblInput;
                }
                else
                {
                    //Bad input
                    throw new Exception("Input needs to be numeric");
                }
                
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private double CalculateSquare(ref string input)
        {
            try
            {
                double dblInput=0;

                if (double.TryParse(input, out dblInput))
                {
                    //Good input
                    input = "4";
                    return dblInput * dblInput;
                }
                else
                {
                    //Bad input
                    throw new Exception("Input needs to be numeric");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void btnDisplayByVal_Click(object sender, EventArgs e)
        {
            try
            {
                lblStatus.Text = string.Empty;
                lblStatus.ForeColor = SystemColors.ControlText;

                lblResult.Text = CalculateSquare(txtInput.Text).ToString("n2");                
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.Red;
            }
        }

        private void btnDisplayByRef_Click(object sender, EventArgs e)
        {
            try
            {
                lblStatus.Text = string.Empty;
                lblStatus.ForeColor = SystemColors.Control;

                string number = txtInput.Text;

                lblResult.Text = CalculateSquare(ref number).ToString("n2");

                lblStatus.Text = number;
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.Red;
            }
        }
    }
}
