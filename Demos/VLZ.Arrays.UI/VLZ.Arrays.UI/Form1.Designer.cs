﻿namespace VLZ.Arrays.UI
{
    partial class frmArrays
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCreat1d = new System.Windows.Forms.Button();
            this.btnDisplay1d = new System.Windows.Forms.Button();
            this.btnDisplayName2 = new System.Windows.Forms.Button();
            this.btnMakeNumbers = new System.Windows.Forms.Button();
            this.btnForEach = new System.Windows.Forms.Button();
            this.btnCreat2d = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.lstOutput = new System.Windows.Forms.ListBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCreat1d
            // 
            this.btnCreat1d.Location = new System.Drawing.Point(12, 16);
            this.btnCreat1d.Name = "btnCreat1d";
            this.btnCreat1d.Size = new System.Drawing.Size(110, 23);
            this.btnCreat1d.TabIndex = 0;
            this.btnCreat1d.Text = "Create 1-Dim";
            this.btnCreat1d.UseVisualStyleBackColor = true;
            this.btnCreat1d.Click += new System.EventHandler(this.btnCreat1d_Click);
            // 
            // btnDisplay1d
            // 
            this.btnDisplay1d.Location = new System.Drawing.Point(12, 45);
            this.btnDisplay1d.Name = "btnDisplay1d";
            this.btnDisplay1d.Size = new System.Drawing.Size(110, 23);
            this.btnDisplay1d.TabIndex = 1;
            this.btnDisplay1d.Text = "Display 1-Dim";
            this.btnDisplay1d.UseVisualStyleBackColor = true;
            this.btnDisplay1d.Click += new System.EventHandler(this.btnDisplay1d_Click);
            // 
            // btnDisplayName2
            // 
            this.btnDisplayName2.Location = new System.Drawing.Point(12, 74);
            this.btnDisplayName2.Name = "btnDisplayName2";
            this.btnDisplayName2.Size = new System.Drawing.Size(110, 23);
            this.btnDisplayName2.TabIndex = 2;
            this.btnDisplayName2.Text = "Change Name 2";
            this.btnDisplayName2.UseVisualStyleBackColor = true;
            this.btnDisplayName2.Click += new System.EventHandler(this.btnDisplayName2_Click);
            // 
            // btnMakeNumbers
            // 
            this.btnMakeNumbers.Location = new System.Drawing.Point(12, 103);
            this.btnMakeNumbers.Name = "btnMakeNumbers";
            this.btnMakeNumbers.Size = new System.Drawing.Size(110, 23);
            this.btnMakeNumbers.TabIndex = 3;
            this.btnMakeNumbers.Text = "Make Numbers";
            this.btnMakeNumbers.UseVisualStyleBackColor = true;
            this.btnMakeNumbers.Click += new System.EventHandler(this.btnMakeNumbers_Click);
            // 
            // btnForEach
            // 
            this.btnForEach.Location = new System.Drawing.Point(12, 132);
            this.btnForEach.Name = "btnForEach";
            this.btnForEach.Size = new System.Drawing.Size(110, 23);
            this.btnForEach.TabIndex = 4;
            this.btnForEach.Text = "For Each";
            this.btnForEach.UseVisualStyleBackColor = true;
            this.btnForEach.Click += new System.EventHandler(this.btnForEach_Click);
            // 
            // btnCreat2d
            // 
            this.btnCreat2d.Location = new System.Drawing.Point(12, 161);
            this.btnCreat2d.Name = "btnCreat2d";
            this.btnCreat2d.Size = new System.Drawing.Size(110, 23);
            this.btnCreat2d.TabIndex = 5;
            this.btnCreat2d.Text = "Create 2-Dim";
            this.btnCreat2d.UseVisualStyleBackColor = true;
            this.btnCreat2d.Click += new System.EventHandler(this.btnCreat2d_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(12, 190);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(110, 23);
            this.button7.TabIndex = 6;
            this.button7.Text = "button7";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(12, 248);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(110, 23);
            this.button8.TabIndex = 7;
            this.button8.Text = "button8";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(12, 219);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(110, 23);
            this.button9.TabIndex = 8;
            this.button9.Text = "button9";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(12, 277);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(110, 23);
            this.button10.TabIndex = 9;
            this.button10.Text = "button10";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // lstOutput
            // 
            this.lstOutput.FormattingEnabled = true;
            this.lstOutput.Location = new System.Drawing.Point(172, 12);
            this.lstOutput.Name = "lstOutput";
            this.lstOutput.Size = new System.Drawing.Size(359, 290);
            this.lstOutput.TabIndex = 10;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 449);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(927, 22);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // frmArrays
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PaleTurquoise;
            this.ClientSize = new System.Drawing.Size(927, 471);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lstOutput);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.btnCreat2d);
            this.Controls.Add(this.btnForEach);
            this.Controls.Add(this.btnMakeNumbers);
            this.Controls.Add(this.btnDisplayName2);
            this.Controls.Add(this.btnDisplay1d);
            this.Controls.Add(this.btnCreat1d);
            this.Name = "frmArrays";
            this.Text = "Arrays";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCreat1d;
        private System.Windows.Forms.Button btnDisplay1d;
        private System.Windows.Forms.Button btnDisplayName2;
        private System.Windows.Forms.Button btnMakeNumbers;
        private System.Windows.Forms.Button btnForEach;
        private System.Windows.Forms.Button btnCreat2d;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.ListBox lstOutput;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
    }
}

