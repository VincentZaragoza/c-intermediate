﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VLZ.Arrays.UI
{
    public partial class frmArrays : Form
    {

        //Dimension a ames array by specifying the contents
        string[] names = { "Fred", "Barney", "Wilma", "Betty", "Pebbles", "Bam-Bam" };

        //Dimension a ames array by specifying the size
        string[] names2 = new string[6];

        public frmArrays()
        {
            InitializeComponent();
        }

        private void btnCreat1d_Click(object sender, EventArgs e)
        {
            lstOutput.Items.Clear();

            lstOutput.Items.Add(names[0]);
            lstOutput.Items.Add(names[1]);
            lstOutput.Items.Add(names[2]);
            lstOutput.Items.Add(names[3]);
            lstOutput.Items.Add(names[4]);
            lstOutput.Items.Add(names[5]);

        }

        private void btnDisplay1d_Click(object sender, EventArgs e)
        {
            DisplayContents(names);
        }

        private void DisplayContents(string[] array)
        {
            lstOutput.Items.Clear();
            for (int i = 0; i < array.Length; i++)
            {
                lstOutput.Items.Add(array[i]);
            }
        }

        private void DisplayContents(string[] array, bool useforeach = true)
        {
            lstOutput.Items.Clear();

            foreach(string name in array)
            {
                lstOutput.Items.Add(name);
            }
        }

        private void DisplayContents(double[] array)
        {
            lstOutput.Items.Clear();
            for (int i = 0; i < array.Length; i++)
            {
                lstOutput.Items.Add(array[i]);
            }
        }

        private void DisplayContents(int[,] array)
        {
            lstOutput.Items.Clear();
            for (int row = 0; row <= array.GetUpperBound(0); row++)
            {
                for (int col = 0; col <= array.GetUpperBound(1); col++)
                {
                    lstOutput.Items.Add(array[row,col]);
                }
            }
        }


        private void btnDisplayName2_Click(object sender, EventArgs e)
        {
            //Change the values in names2(empty right now)
            names2[0] = "Batman";
            names2[1] = "Superman";
            names2[2] = "Aquaman";
            names2[3] = "Wonder Woman";
            names2[4] = "Flash";
            names2[5] = string.Empty;

            //Display
            DisplayContents(names2);

        }

        private void btnMakeNumbers_Click(object sender, EventArgs e)
        {
            double[] numbers = new double[50];
            Random random = new Random();

            for (int i = 0; i < 50; i ++)
            {
                

                double newnumber = random.NextDouble() * 10;
                numbers[i] = newnumber;

            }

            DisplayContents(numbers);
        }

        private void btnForEach_Click(object sender, EventArgs e)
        {
            DisplayContents(names, true);
        }

        private void btnCreat2d_Click(object sender, EventArgs e)
        {
            int[,] numbers = new int[2,6];

            numbers[0, 0] = 2;
            numbers[0, 1] = 3;
            numbers[0, 2] = 1234;
            numbers[0, 3] = 123;
            numbers[0, 4] = 12342;
            numbers[0, 5] = 21234;

            numbers[1, 0] = 10;
            numbers[1, 1] = 11;
            numbers[1, 2] = 12;
            numbers[1, 3] = 13;
            numbers[1, 4] = 14;
            numbers[1, 5] = 15;

            DisplayContents(numbers);
        }
    }
}
