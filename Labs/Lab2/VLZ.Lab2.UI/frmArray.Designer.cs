﻿namespace VLZ.Lab2.UI
{
    partial class frmArrays
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAdd1 = new System.Windows.Forms.Button();
            this.btnAdd2 = new System.Windows.Forms.Button();
            this.btnAdd3 = new System.Windows.Forms.Button();
            this.btnAdd4 = new System.Windows.Forms.Button();
            this.btnAdd5 = new System.Windows.Forms.Button();
            this.btnDisplayArray = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblAvg = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblSum = new System.Windows.Forms.Label();
            this.lblElemNum = new System.Windows.Forms.Label();
            this.lstDisplay = new System.Windows.Forms.ListBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.txtInput = new System.Windows.Forms.TextBox();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAdd1
            // 
            this.btnAdd1.Location = new System.Drawing.Point(41, 65);
            this.btnAdd1.Name = "btnAdd1";
            this.btnAdd1.Size = new System.Drawing.Size(111, 20);
            this.btnAdd1.TabIndex = 0;
            this.btnAdd1.Text = "Add to Array #1";
            this.btnAdd1.UseVisualStyleBackColor = true;
            // 
            // btnAdd2
            // 
            this.btnAdd2.Location = new System.Drawing.Point(41, 97);
            this.btnAdd2.Name = "btnAdd2";
            this.btnAdd2.Size = new System.Drawing.Size(111, 20);
            this.btnAdd2.TabIndex = 1;
            this.btnAdd2.Text = "Add to Array #2";
            this.btnAdd2.UseVisualStyleBackColor = true;
            // 
            // btnAdd3
            // 
            this.btnAdd3.Location = new System.Drawing.Point(41, 129);
            this.btnAdd3.Name = "btnAdd3";
            this.btnAdd3.Size = new System.Drawing.Size(111, 20);
            this.btnAdd3.TabIndex = 2;
            this.btnAdd3.Text = "Add to Array #3";
            this.btnAdd3.UseVisualStyleBackColor = true;
            // 
            // btnAdd4
            // 
            this.btnAdd4.Location = new System.Drawing.Point(41, 161);
            this.btnAdd4.Name = "btnAdd4";
            this.btnAdd4.Size = new System.Drawing.Size(111, 20);
            this.btnAdd4.TabIndex = 3;
            this.btnAdd4.Text = "Add to Array #4";
            this.btnAdd4.UseVisualStyleBackColor = true;
            // 
            // btnAdd5
            // 
            this.btnAdd5.Location = new System.Drawing.Point(41, 193);
            this.btnAdd5.Name = "btnAdd5";
            this.btnAdd5.Size = new System.Drawing.Size(111, 20);
            this.btnAdd5.TabIndex = 4;
            this.btnAdd5.Text = "Add to Array #5";
            this.btnAdd5.UseVisualStyleBackColor = true;
            // 
            // btnDisplayArray
            // 
            this.btnDisplayArray.Location = new System.Drawing.Point(427, 39);
            this.btnDisplayArray.Name = "btnDisplayArray";
            this.btnDisplayArray.Size = new System.Drawing.Size(111, 20);
            this.btnDisplayArray.TabIndex = 5;
            this.btnDisplayArray.Text = "Display Array";
            this.btnDisplayArray.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(463, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "# of Elements:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(430, 158);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Average of Elements:";
            // 
            // lblAvg
            // 
            this.lblAvg.AllowDrop = true;
            this.lblAvg.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAvg.Location = new System.Drawing.Point(438, 178);
            this.lblAvg.Name = "lblAvg";
            this.lblAvg.Size = new System.Drawing.Size(100, 19);
            this.lblAvg.TabIndex = 8;
            this.lblAvg.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(449, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(89, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Sum of Elements:";
            // 
            // lblSum
            // 
            this.lblSum.AllowDrop = true;
            this.lblSum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSum.Location = new System.Drawing.Point(438, 132);
            this.lblSum.Name = "lblSum";
            this.lblSum.Size = new System.Drawing.Size(100, 19);
            this.lblSum.TabIndex = 10;
            this.lblSum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblElemNum
            // 
            this.lblElemNum.AllowDrop = true;
            this.lblElemNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblElemNum.Location = new System.Drawing.Point(438, 86);
            this.lblElemNum.Name = "lblElemNum";
            this.lblElemNum.Size = new System.Drawing.Size(100, 19);
            this.lblElemNum.TabIndex = 11;
            this.lblElemNum.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lstDisplay
            // 
            this.lstDisplay.FormattingEnabled = true;
            this.lstDisplay.Location = new System.Drawing.Point(182, 39);
            this.lstDisplay.Name = "lstDisplay";
            this.lstDisplay.Size = new System.Drawing.Size(206, 95);
            this.lstDisplay.TabIndex = 12;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 230);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(568, 22);
            this.statusStrip1.TabIndex = 13;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // txtInput
            // 
            this.txtInput.Location = new System.Drawing.Point(41, 39);
            this.txtInput.Name = "txtInput";
            this.txtInput.Size = new System.Drawing.Size(111, 20);
            this.txtInput.TabIndex = 14;
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // frmArrays
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 252);
            this.Controls.Add(this.txtInput);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lstDisplay);
            this.Controls.Add(this.lblElemNum);
            this.Controls.Add(this.lblSum);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblAvg);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnDisplayArray);
            this.Controls.Add(this.btnAdd5);
            this.Controls.Add(this.btnAdd4);
            this.Controls.Add(this.btnAdd3);
            this.Controls.Add(this.btnAdd2);
            this.Controls.Add(this.btnAdd1);
            this.Name = "frmArrays";
            this.Text = "Arrays";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAdd1;
        private System.Windows.Forms.Button btnAdd2;
        private System.Windows.Forms.Button btnAdd3;
        private System.Windows.Forms.Button btnAdd4;
        private System.Windows.Forms.Button btnAdd5;
        private System.Windows.Forms.Button btnDisplayArray;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblAvg;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblSum;
        private System.Windows.Forms.Label lblElemNum;
        private System.Windows.Forms.ListBox lstDisplay;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.TextBox txtInput;
    }
}

