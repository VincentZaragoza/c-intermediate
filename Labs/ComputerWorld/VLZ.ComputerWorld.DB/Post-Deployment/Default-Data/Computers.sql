﻿BEGIN
	INSERT INTO dbo.tblComputer(Id,Manufacturer,Model,Cost,EquipmentTypeID,HardDriveSize,ProcessorSpeed)
	VALUES
	(1,'DELL','6100',499,1,2048,3.1),
	(2,'HP','4100',799,1,4192,5),
	(3,'MSI','Stealth',2049,1,16000,7.0)
END 