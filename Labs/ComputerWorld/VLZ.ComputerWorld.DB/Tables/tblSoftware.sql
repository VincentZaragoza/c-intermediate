﻿CREATE TABLE [dbo].[tblSoftware]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Name] VARCHAR(50) NOT NULL, 
    [Size] DECIMAL(11, 2) NOT NULL
)
