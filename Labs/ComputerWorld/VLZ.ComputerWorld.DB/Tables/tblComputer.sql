﻿CREATE TABLE [dbo].[tblComputer]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [Manufacturer] VARCHAR(50) NOT NULL, 
    [Model] VARCHAR(50) NOT NULL, 
    [Cost] DECIMAL(11, 2) NOT NULL, 
    [EquipmentTypeID] INT NOT NULL, 
    [HardDriveSize] INT NOT NULL, 
    [ProcessorSpeed] DECIMAL(5, 2) NULL
)
