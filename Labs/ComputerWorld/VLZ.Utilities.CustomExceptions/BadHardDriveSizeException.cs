﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VLZ.Utilities.CustomExceptions
{
    public class BadHardDriveSizeException : Exception
    {
        //Create method 1 Constructor - hard code the error message 
        public BadHardDriveSizeException() : base("Hard drive size must be positive.")
        {

        }
      
        //Crete Method 2 constructor - allow for the developer to send a custom message
        public BadHardDriveSizeException(string message) : base(message)
        {

        }
      
    }
}
