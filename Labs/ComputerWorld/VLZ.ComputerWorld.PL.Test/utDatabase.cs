﻿using System;
using System.Data.SqlClient;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VLZ.ComputerWorld.PL.Test;

namespace VLZ.ComputerWorld.PL.Test
{
    [TestClass]
    public class utDatabase
    {
        [TestMethod]
        public void InsertTest()
        {
            Database db = new Database("Data Source=(localdb)\\ProjectsV13;Initial Catalog=ComputerWorld;Integrated Security=True;Connect Timeout=60;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

            string sql = "INSERT INTO tblEquipmentType (Id,Name) VALUES(@Id,@Name)";

            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = sql;

            //Set parameter values;
            cmd.Parameters.AddWithValue("@Id", -1);
            cmd.Parameters.AddWithValue("@Name", "FakeType");

            int actual = db.Insert(cmd);
            db = null;

            // Do something to test that it worked
            int expected = 1;
            Assert.AreEqual(expected, actual);
        }
    }
}
