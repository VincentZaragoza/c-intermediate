﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VLZ.ComputerWorld.BL
{
    public class Software
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string name;

        public string Name
        {
            get { return name; }
            set { string name = value; }
        }

        private double size;

        public double Size
        {
            get { return size; }
            set { size = value; }
        }

        public Software()
        {

        }

        public Software(int _id,string _name, double _size)
        {
            id = _id;
            name = _name;
            size = _size;
        }





    }
}
