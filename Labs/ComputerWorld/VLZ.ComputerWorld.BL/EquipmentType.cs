﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VLZ.ComputerWorld.PL;

namespace VLZ.ComputerWorld.BL
{
    public class EquipmentType
    {
      
        public enum Types
        {
            Computer = 0,
            Projecter = 1,
            Printer = 2,
            Chair = 3,
            Widget = 4
        }

        private int id;
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        public int Delete()
        {
            try
            {
                Database database = new Database(Properties.Settings.Default.ConnectionString);
                string sql = "DELETe from tblEquipmentType where Id = @Id";
                SqlCommand cmd = new SqlCommand(sql);

                cmd.Parameters.AddWithValue("@Id", id);
                int result = database.Delete(cmd);
                database = null;
                return result;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }


        public bool LoadById()
        {
            try
            {
                Database database = new Database(Properties.Settings.Default.ConnectionString);

                string sql = "select *from tblEquipmentType where Id = @Id";

                SqlCommand cmd = new SqlCommand(sql);

                cmd.Parameters.AddWithValue("@Id", id);

                DataTable dataTable = database.Select(cmd);

                database = null;

                return (dataTable.Rows.Count == 0);                
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public int Update()
        {
            try
            {
                Database database = new Database(Properties.Settings.Default.ConnectionString);
                
                //try to select row                
                if (LoadById())
                {
                    string sql = "UPDATE tblEquipmentType SET Name = @Name where Id = @Id";

                    SqlCommand cmd = new SqlCommand(sql);                    
                    cmd.Parameters.AddWithValue("@Id", id);
                    cmd.Parameters.AddWithValue("@Name", name);
                    int result = database.Update(cmd);
                    database = null;    
                    return result;
                }
                else
                {
                    Insert();
                    return 1;
                }
                                
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public bool Insert()
        {
            try
            {
                Database database = new Database(Properties.Settings.Default.ConnectionString);

                SqlCommand cmd = new SqlCommand();

                string sql = "INSERT INTO tblEquipmentType (Id,Name) " + 
                             "Values (@Id,@Name)";

                cmd.CommandText = sql;

                // Add the parameter values
                cmd.Parameters.AddWithValue("@Id", id);
                cmd.Parameters.AddWithValue("@Name", name);

                int result = database.Insert(cmd);

                database = null;
                return (result == 0 ? false : true);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

    }
}
