﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VLZ.ComputerWorld.PL;
using System.Xml.Serialization;
using System.IO;
using System.Data;

namespace VLZ.ComputerWorld.BL
{
    public class ComputerList : List<Computer>
    {
        public void Load()
        {
            try
            {
                Database database = new Database(Properties.Settings.Default.ConnectionString);
                DataTable dataTable = new DataTable();

                // retrieve the data
                string sql = "Select * from tblComputer";
                dataTable = database.Select(sql);

                this.Clear();

                //Fill this class with data from the datatable
                foreach (DataRow dr in dataTable.Rows)
                {
                    Computer computer = new Computer();
                    computer.ID = (int)dr["Id"];
                    computer.Manufacturer = (string)dr["Manufacturer"];
                    computer.Model = (string)dr["Model"];
                    computer.HardDriveSize = (int)dr["HardDriveSize"];
                    computer.ProcessorSpeed = Convert.ToDouble(dr["ProcessorSpeed"]);
                    computer.EquipmentTypeId = (EquipmentType.Types)dr["EquipmentTypeId"];
                    computer.Cost = Convert.ToDouble(dr["Cost"]);
                    Add(computer);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void LoadXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Computer>));
            TextReader otr = new StreamReader("computers.xml");
            this.AddRange((List<Computer>)serializer.Deserialize(otr));
            otr.Close();
            otr = null;
        }

        public void SaveXML()
        {
            try
            {
                FileIO fileIO = new FileIO("computers.xml");
                fileIO.Delete();

                XmlSerializer serializer = new XmlSerializer(typeof(List<Computer>));
                TextWriter otw = new StreamWriter(fileIO.FileName);

                // Generate the xml and put in the file.
                serializer.Serialize(otw, this);
                otw.Close();
                otw = null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public void Populate()
        {
            Computer computer;

            computer = new Computer(this.Count,"HP","5200",499,EquipmentType.Types.Computer,2048,6.0);
            computer.AddSoftware("Slack", 32);
            computer.AddSoftware("Visual Studio Code", 10);
            this.Add(computer);

            computer = new Computer(this.Count, "Compaq", "4200", 699, EquipmentType.Types.Computer, 4192, 2.5);
            computer.AddSoftware("Emulator", 62);
            computer.AddSoftware("Postman", 20);
            this.Add(computer);

            computer = new Computer(this.Count, "MSI", "Stealth", 2049, EquipmentType.Types.Computer, 1600, 7);
            computer.AddSoftware("VR-Chat", 48);
            computer.AddSoftware("Beat Sabre", 10);
            this.Add(computer);
        }
    }
}
