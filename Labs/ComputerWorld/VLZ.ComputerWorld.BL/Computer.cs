﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VLZ.Utilities.CustomExceptions;
using VLZ.ComputerWorld.IL;
using VLZ.ComputerWorld.PL;
using System.Data.SqlClient;

namespace VLZ.ComputerWorld.BL
{
    public class Computer : Equipment, IComputer
    {
        private int hardDriveSize;

        public int HardDriveSize
        {
            get { return hardDriveSize; }
            set
            {
                try
                {
                    if (value >= 0)
                    {
                        hardDriveSize = value;
                    }
                    else
                    {
                        hardDriveSize = 0;
                        //throw new Exception("Hard drive size must be positive");
                        //throw new BadHardDriveSizeException();
                        //or
                        throw new BadHardDriveSizeException("Bad hard drive size.");
                    }
                }

                catch(BadHardDriveSizeException ex)
                {
                    throw ex;
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
        }

        private double processorSpeed;

        public double ProcessorSpeed
        {
            get { return processorSpeed; }
            set { processorSpeed = value; }
        }

        private SoftwareList softwareList;

        public SoftwareList SoftwareList
        {
            get { return softwareList; }
            set { softwareList = value; }
        }

        //Default empty constructor
        public Computer()
        {
            softwareList = new SoftwareList();
        }

        //Custom constructor
        public Computer(int _id, string _manufacturer,
                        string _model, double _cost,
                        EquipmentType.Types _equipmentTypeId,int _hardDriveSize,
                        double _processorSpeed)
        {
            softwareList = new SoftwareList();

            hardDriveSize = _hardDriveSize;
            processorSpeed = _processorSpeed;

            ID = _id;
            base.ID = _id;
            base.Manufacturer = _manufacturer;
            base.Model = _model;
            base.Cost = _cost;
            base.EquipmentTypeId = _equipmentTypeId;


        }

        public bool Insert()
        {
            try
            {
                Database database = new Database();
                SqlCommand cmd = new SqlCommand();

                string sql = "insert into tblComputer (Id,Maufacturer,Model,Cost,HardDriveSize.ProcessorSpeed,EquipmentTypeId) +" +
                            "values(@Id,@Maufacturer,@Model,@Cost,@HardDriveSize.@ProcessorSpeed,@EquipmentTypeId)";
                cmd.CommandText = sql;

                cmd.Parameters.AddWithValue("@Id", this.ID);
                cmd.Parameters.AddWithValue("@Manufacturer", this.Manufacturer);
                cmd.Parameters.AddWithValue("@Model", this.Model);
                cmd.Parameters.AddWithValue("@Cost", this.Cost);
                cmd.Parameters.AddWithValue("@ProcessorSpeed", this.ProcessorSpeed);
                cmd.Parameters.AddWithValue("@HardDriveSize", this.HardDriveSize);
                cmd.Parameters.AddWithValue("@EquipmentTypeId", this.EquipmentTypeId);

                int result = database.Insert(cmd);
                database = null;
                return (result == 0 ? false : true);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public int Update(string userid)
        {
            try
            {
                Database database = new Database();
                SqlCommand cmd = new SqlCommand();

                string sql = "Update tblComputer set Maufacturer = @Manufaturer,Model=@Model,Cost,HardDriveSize=@HardDriveSize.ProcessorSpeed@ProcessorSpeed,EquipmentTypeId" +
                             "where Id=" + this.ID;
                cmd.CommandText = sql;

                cmd.Parameters.AddWithValue("@Id", this.ID);
                int result = database.Update(cmd);
                database = null;
                return result;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        public int Delete()
        {
            try
            {
                Database database = new Database();
                SqlCommand cmd = new SqlCommand();

                string sql = "Delete from tblComputer where Id = @Id";
                cmd.CommandText = sql;

                cmd.Parameters.AddWithValue("@Id", this.ID);
                int result = database.Delete(cmd);
                database = null;
                return result;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Populate()
        {
            try
            {
                this.ID = 1;
                this.Manufacturer = "Dell";
                this.Model = "6100";
                this.ProcessorSpeed = 2.4;
                this.Cost = 600;
                this.EquipmentTypeId = EquipmentType.Types.Computer;
                this.HardDriveSize = 1024;

                //Add some software
                this.AddSoftware("Word", 32);
                this.AddSoftware("Discord", 5);
                this.AddSoftware("Steam", 1500);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void AddSoftware(string name, double size)
        {
            this.softwareList.Add(new Software(this.softwareList.Count, name, size));
        }

        
    }
}
