﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using VLZ.ComputerWorld.PL;

namespace VLZ.ComputerWorld.BL
{
    public class EquipmentTypeList : List<EquipmentType>
    {
        const string fileName = "equipmenttypes.xml";

        public void Save()
        {
            try
            {
                foreach(EquipmentType e in this)
                {
                    e.Update();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Load()
        {
            try
            {
                Database database = new Database(Properties.Settings.Default.ConnectionString);
                DataTable dataTable = new DataTable();

                // retrieve the data
                string sql = "Select * from tblEquipmentType";
                dataTable = database.Select(sql);

                this.Clear();

                //Fill this class with data from the datatable
                foreach(DataRow dr in dataTable.Rows)
                {
                    EquipmentType et = new EquipmentType();
                    et.ID = (int)dr["Id"];
                    et.Name = dr["Name"].ToString();
                    Add(et);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void LoadXML()
        {
            this.Clear();
            XmlSerializer serializer = new XmlSerializer(typeof(List<EquipmentType>));
            TextReader otr = new StreamReader(fileName);
            this.AddRange((List<EquipmentType>)serializer.Deserialize(otr));
            otr.Close();
            otr = null;
        }

        

        public void Populate()
        {
            if (File.Exists(fileName))
            {
                //Use xml file.
                LoadXML();
            }
            else
            {
                //Use the enumeration.
                var types = Enum.GetNames(typeof(EquipmentType.Types));

                 foreach(var t in types)
                {
                    this.Add(new EquipmentType { ID = this.Count + 1, Name = t });
                }

                //Create xml file
                SaveXML();
            }
        }

        public void SaveXML()
        {
            try
            {
                FileIO fileIO = new FileIO(fileName);
                fileIO.Delete();

                XmlSerializer serializer = new XmlSerializer(typeof(List<EquipmentType>));
                TextWriter otw = new StreamWriter(fileIO.FileName);

                // Generate the xml and put in the file.
                serializer.Serialize(otw, this);
                otw.Close();
                otw = null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
    }

}
