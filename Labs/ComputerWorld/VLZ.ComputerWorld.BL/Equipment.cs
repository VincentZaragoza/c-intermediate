﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VLZ.ComputerWorld.BL
{
    public class Equipment
    {
        private int id;

        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private  EquipmentType.Types equipmentTypeId;

        public EquipmentType.Types EquipmentTypeId
        {
            get { return equipmentTypeId; }
            set { equipmentTypeId = value; }
        }

        private double cost;

        public double Cost
        {
            get { return cost; }
            set { double cost = value; }
        }

        private string manufacturer;

        public string Manufacturer
        {
            get { return manufacturer; }
            set { manufacturer = value; }
        }

        private string model;

        public string Model
        {
            get { return model; }
            set { model = value; }
        }
    }
}
