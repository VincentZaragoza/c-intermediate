﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace VLZ.ComputerWorld.IL
{
    public interface IComputer
    {
        int HardDriveSize { get; set; }

        bool Insert();
                     
        int Update(string userid);

    }
}
