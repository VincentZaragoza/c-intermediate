﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VLZ.ComputerWorld.BL;

namespace VLZ.ComputerWorld.UI
{
    public partial class frmMaintainTypes : Form
    {
        EquipmentTypeList equipmentTypes;

        public frmMaintainTypes()
        {
            InitializeComponent();
        }

        public frmMaintainTypes(EquipmentTypeList _equipmentTypes)
        {
            InitializeComponent();
            equipmentTypes = _equipmentTypes;
            BindEquipmentTypes();
                
        }
        private void BindEquipmentTypes()
        {
            cbxType.DataSource = null;
            cbxType.DataSource = equipmentTypes;
            cbxType.DisplayMember = "Name";
            cbxType.ValueMember = "ID";
        }

        private void cbxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(cbxType.SelectedIndex >- 1)
            txtType.Text = equipmentTypes[cbxType.SelectedIndex].Name;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {

                //Check to see if any rows exist that matches on the new name
                if(!equipmentTypes.Any(et => et.Name == txtType.Text))
                {
                    equipmentTypes.Add(new EquipmentType { ID = equipmentTypes.Count + 1, Name = txtType.Text });
                    BindEquipmentTypes();
                    cbxType.SelectedIndex = equipmentTypes.Count - 1;
                    lblStatus.Text = txtType.Text + " added succesfully";
                    lblStatus.ForeColor = Color.BlueViolet;

                }
                else
                {
                    throw new Exception(txtType.Text + " alreadt exists. Try again");
                }
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.Red;                
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                equipmentTypes[cbxType.SelectedIndex].Name = txtType.Text;
                BindEquipmentTypes();
                lblStatus.Text = txtType.Text + " updated succesfully";
                lblStatus.ForeColor = Color.BlueViolet;
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.Red;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                equipmentTypes.RemoveAt(cbxType.SelectedIndex);
                BindEquipmentTypes();
                lblStatus.Text = txtType.Text + " deleted succesfully";
                lblStatus.ForeColor = Color.BlueViolet;
                cbxType.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.Red;
            }

        }

        private void frmMaintainTypes_FormClosing(object sender, FormClosingEventArgs e)
        {
            equipmentTypes.SaveXML();
            equipmentTypes.Save();
        }
    }
}
