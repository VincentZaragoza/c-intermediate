﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VLZ.ComputerWorld.BL;


namespace VLZ.ComputerWorld.UI
{
    public partial class frmComputerWorld : Form 
    {        
        Computer computer;
        ComputerList computerList;
        EquipmentTypeList equipmentTypes;       

        public frmComputerWorld()
        {
            InitializeComponent();
        }

        private void btnMakeComputer_Click(object sender, EventArgs e)
        {
            try
            {
                computer = new Computer();
                computer.Populate();

                if (computerList == null)
                    computerList = new ComputerList();

                computerList.Add(computer);

                Rebind();
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.IndianRed;
            }
        }

        private void Rebind()
        {
            //Bind the computer list to the datagridview
            dgvEquipment.DataSource = null;
            dgvEquipment.DataSource = computerList;

            //Bind computer list to the list box
            lbxComputer.DataSource = null;
            lbxComputer.DataSource = computerList;
            lbxComputer.DisplayMember = "Manufacturer";
            lbxComputer.ValueMember = "Id";
        }

        private void lbxComputer_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (lbxComputer.SelectedIndex > -1)
            {
                //Get the computer selected and show its software
                computer = computerList[lbxComputer.SelectedIndex];
                lbxSoftware.DataSource = null;
                lbxSoftware.DataSource = computer.SoftwareList;
                lbxSoftware.DisplayMember = "Name";
                lbxSoftware.ValueMember = "Id";
            }
        }

        private void btnMakeComputers_Click(object sender, EventArgs e)
        {
            try
            {
                computerList = new ComputerList();
                computerList.Populate();
                Rebind();
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.IndianRed;
            }
        }

        private void frmComputerWorld_Load(object sender, EventArgs e)
        {
            //Fill the combobox with equipment types
            MakeEquipmentTypes();
        }

        private void MakeEquipmentTypes()
        {
            equipmentTypes = new EquipmentTypeList();
            //equipmentTypes.Populate();
            equipmentTypes.Load();
            BindEquipmentTypes();
            
        }

        private void BindEquipmentTypes()
        {
            cbxEquipmentType.DataSource = null;
            cbxEquipmentType.DataSource = equipmentTypes;
            cbxEquipmentType.DisplayMember = "Name";
            cbxEquipmentType.ValueMember = "ID";
        }

        private void btnInsertEquipment_Click(object sender, EventArgs e)
        {
            try
            {

                lblStatus.Text = string.Empty;
                //Get the values from screen
                computer = new Computer();
                computer.Manufacturer = txtManufacturer.Text;
                computer.Model = txtModel.Text;

                double cost;
                ValidateDouble(txtCost, out cost,"Invalid Cost");

                double speed;
                ValidateDouble(txtProcessorSpeed, out speed, "Invalid Processor Speed");

                int size;
                ValidateInteger(txtHddSize, out size, "Invalid Hard Drive Size");

                //Replace This:
                //Bad computer.ProcessorSpeed = Convert.todouble(txtProcessorSpeed.text);
                computer.EquipmentTypeId = (EquipmentType.Types)cbxEquipmentType.SelectedIndex;

                //Call the insert method on list
                if(computerList==null)
                {
                    computerList = new ComputerList();
                    computer.ID = 1;
                }
                else
                {
                    computer.ID = computerList.Max(c => c.ID) +1;
                }


                computerList.Add(computer);
                computer.Insert();
                Rebind();


            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.IndianRed;
            }
        }

        private void ValidateInteger(TextBox source, out int target, string message)
        {
            try
            {
                int value1;
                if(int.TryParse(source.Text,out value1))
                {
                    target = value1;
                    source.BackColor = SystemColors.Window;
                }
                else
                {
                    source.BackColor = Color.LightYellow;
                    source.Focus();
                    source.SelectAll();
                    throw new Exception(message);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private void ValidateDouble(TextBox source, out double target,string mesage)
        {
            try
            {
                double value1;
                if (double.TryParse(source.Text, out value1))
                {
                    target = value1;
                    source.BackColor = SystemColors.Window;
                }
                else
                {
                    source.BackColor = Color.LightYellow;
                    source.Focus();
                    source.SelectAll();
                    throw new Exception(mesage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void btnDefaultData_Click(object sender, EventArgs e)
        {
            txtManufacturer.Text = "Dell";
            txtCost.Text = "499.95";
            txtModel.Text = "6100";
            txtHddSize.Text = "1200";
            txtProcessorSpeed.Text = "3";
            cbxEquipmentType.SelectedIndex = 0;  

        }
        private void btnUpdateEquipment_Click(object sender, EventArgs e)
        {
            try
            {
                lblStatus.Text = string.Empty;
                //Get the values from screen
                computer = computerList[dgvEquipment.CurrentRow.Index];

                computer.Manufacturer = txtManufacturer.Text;
                computer.Model = txtModel.Text;

                double cost;
                ValidateDouble(txtCost, out cost, "Invalid Cost");
                computer.Cost = cost;

                double speed;
                ValidateDouble(txtProcessorSpeed, out speed, "Invalid Processor Speed");

                int size;
                ValidateInteger(txtHddSize, out size, "Invalid Hard Drive Size");

                //Replace This:
                //Bad computer.ProcessorSpeed = Convert.todouble(txtProcessorSpeed.text);
                computer.EquipmentTypeId = (EquipmentType.Types)cbxEquipmentType.SelectedIndex;

                //Call the insert method on list
                if (computerList == null)
                {
                    computerList = new ComputerList();
                    computer.ID = 1;
                }
                else
                {
                    computer.ID = computerList.Max(c => c.ID) + 1;
                }
                
                computer.Update(string.Empty);
                Rebind();
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.IndianRed;
            }
        }

        private void dgvEquipment_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if(dgvEquipment.CurrentRow != null)
                {
                    //Get 1 from the list
                    computer = computerList[dgvEquipment.CurrentRow.Index];

                    //Put the values in controls
                    lblId.Text = computer.ID.ToString();
                    txtManufacturer.Text = computer.Manufacturer;
                    txtModel.Text = computer.Model;
                    txtCost.Text = computer.Cost.ToString();
                    txtHddSize.Text = computer.HardDriveSize.ToString();
                    txtProcessorSpeed.Text = computer.ProcessorSpeed.ToString();

                    cbxEquipmentType.SelectedIndex = (int)computer.EquipmentTypeId;
                }

            }
            catch (Exception ex)
            {
                //lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.IndianRed;
            }
        }

        private void btnDeleteEquipment_Click(object sender, EventArgs e)
        {
            try
            {
                if(dgvEquipment.CurrentRow != null)
                {
                    computer = computerList[dgvEquipment.CurrentRow.Index];
                    computerList.Remove(computer);
                    //OR
                    //Remove at a paticular index
                    //computerList.RemoveAt(dgvEquipment.CurrentRow.Index);

                    computer.Delete();
                    Rebind();
                }

            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.IndianRed;
            }
        }

        private void btnSaveXML_Click(object sender, EventArgs e)
        {
            try
            {
                if (computerList != null)
                    computerList.SaveXML();
                
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.IndianRed;
            }
        }

        private void btnLoadXML_Click(object sender, EventArgs e)
        {
            try
            {
                computerList = new ComputerList();
                computerList.LoadXML();
                Rebind();

            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.IndianRed;
            }
        }

        private void btnMaintainEquipmentTypes_Click(object sender, EventArgs e)
        {
            (new frmMaintainTypes(equipmentTypes)).ShowDialog();
            equipmentTypes.LoadXML();
            equipmentTypes.Load();
            BindEquipmentTypes();
        }

        
        private void btnLoadDB_Click(object sender, EventArgs e)
        {
            try
            {
                computerList = new ComputerList();
                computerList.Load();
                Rebind();

            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.IndianRed;
            }
        }
    }
}
