﻿namespace VLZ.ComputerWorld.UI
{
    partial class frmComputerWorld
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.btnMakeComputer = new System.Windows.Forms.Button();
            this.lbxComputer = new System.Windows.Forms.ListBox();
            this.dgvEquipment = new System.Windows.Forms.DataGridView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lbxSoftware = new System.Windows.Forms.ListBox();
            this.btnMakeComputers = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            this.txtManufacturer = new System.Windows.Forms.TextBox();
            this.txtModel = new System.Windows.Forms.TextBox();
            this.txtCost = new System.Windows.Forms.TextBox();
            this.txtHddSize = new System.Windows.Forms.TextBox();
            this.txtProcessorSpeed = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbxEquipmentType = new System.Windows.Forms.ComboBox();
            this.btnInsertEquipment = new System.Windows.Forms.Button();
            this.btnDefaultData = new System.Windows.Forms.Button();
            this.btnUpdateEquipment = new System.Windows.Forms.Button();
            this.btnDeleteEquipment = new System.Windows.Forms.Button();
            this.btnSaveXML = new System.Windows.Forms.Button();
            this.btnLoadXML = new System.Windows.Forms.Button();
            this.btnMaintainEquipmentTypes = new System.Windows.Forms.Button();
            this.btnLoadDB = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEquipment)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnMakeComputer
            // 
            this.btnMakeComputer.Location = new System.Drawing.Point(12, 33);
            this.btnMakeComputer.Name = "btnMakeComputer";
            this.btnMakeComputer.Size = new System.Drawing.Size(106, 23);
            this.btnMakeComputer.TabIndex = 0;
            this.btnMakeComputer.Text = "Make Computer";
            this.btnMakeComputer.UseVisualStyleBackColor = true;
            this.btnMakeComputer.Click += new System.EventHandler(this.btnMakeComputer_Click);
            // 
            // lbxComputer
            // 
            this.lbxComputer.FormattingEnabled = true;
            this.lbxComputer.Location = new System.Drawing.Point(124, 33);
            this.lbxComputer.Name = "lbxComputer";
            this.lbxComputer.Size = new System.Drawing.Size(150, 264);
            this.lbxComputer.TabIndex = 1;
            this.lbxComputer.SelectedIndexChanged += new System.EventHandler(this.lbxComputer_SelectedIndexChanged);
            // 
            // dgvEquipment
            // 
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White;
            this.dgvEquipment.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEquipment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEquipment.Location = new System.Drawing.Point(280, 33);
            this.dgvEquipment.Name = "dgvEquipment";
            this.dgvEquipment.Size = new System.Drawing.Size(787, 264);
            this.dgvEquipment.TabIndex = 2;
            this.dgvEquipment.SelectionChanged += new System.EventHandler(this.dgvEquipment_SelectionChanged);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 590);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1243, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // lbxSoftware
            // 
            this.lbxSoftware.FormattingEnabled = true;
            this.lbxSoftware.Location = new System.Drawing.Point(1073, 33);
            this.lbxSoftware.Name = "lbxSoftware";
            this.lbxSoftware.Size = new System.Drawing.Size(150, 264);
            this.lbxSoftware.TabIndex = 4;
            // 
            // btnMakeComputers
            // 
            this.btnMakeComputers.Location = new System.Drawing.Point(12, 62);
            this.btnMakeComputers.Name = "btnMakeComputers";
            this.btnMakeComputers.Size = new System.Drawing.Size(106, 23);
            this.btnMakeComputers.TabIndex = 5;
            this.btnMakeComputers.Text = "Make Computers";
            this.btnMakeComputers.UseVisualStyleBackColor = true;
            this.btnMakeComputers.Click += new System.EventHandler(this.btnMakeComputers_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(300, 458);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Hard Drive Size:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(300, 494);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Processor Speed:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(300, 386);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Model:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(301, 314);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(19, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Id.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(300, 422);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(31, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Cost:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(300, 350);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Manufacturer:";
            // 
            // lblId
            // 
            this.lblId.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblId.Location = new System.Drawing.Point(394, 307);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(47, 20);
            this.lblId.TabIndex = 9;
            // 
            // txtManufacturer
            // 
            this.txtManufacturer.Location = new System.Drawing.Point(394, 347);
            this.txtManufacturer.Name = "txtManufacturer";
            this.txtManufacturer.Size = new System.Drawing.Size(200, 20);
            this.txtManufacturer.TabIndex = 12;
            // 
            // txtModel
            // 
            this.txtModel.Location = new System.Drawing.Point(394, 383);
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(200, 20);
            this.txtModel.TabIndex = 12;
            // 
            // txtCost
            // 
            this.txtCost.Location = new System.Drawing.Point(394, 419);
            this.txtCost.Name = "txtCost";
            this.txtCost.Size = new System.Drawing.Size(200, 20);
            this.txtCost.TabIndex = 12;
            // 
            // txtHddSize
            // 
            this.txtHddSize.Location = new System.Drawing.Point(394, 455);
            this.txtHddSize.Name = "txtHddSize";
            this.txtHddSize.Size = new System.Drawing.Size(200, 20);
            this.txtHddSize.TabIndex = 12;
            // 
            // txtProcessorSpeed
            // 
            this.txtProcessorSpeed.Location = new System.Drawing.Point(394, 491);
            this.txtProcessorSpeed.Name = "txtProcessorSpeed";
            this.txtProcessorSpeed.Size = new System.Drawing.Size(200, 20);
            this.txtProcessorSpeed.TabIndex = 12;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(300, 530);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(87, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Equipment Type:";
            // 
            // cbxEquipmentType
            // 
            this.cbxEquipmentType.FormattingEnabled = true;
            this.cbxEquipmentType.Location = new System.Drawing.Point(394, 527);
            this.cbxEquipmentType.Name = "cbxEquipmentType";
            this.cbxEquipmentType.Size = new System.Drawing.Size(200, 21);
            this.cbxEquipmentType.TabIndex = 13;
            // 
            // btnInsertEquipment
            // 
            this.btnInsertEquipment.Location = new System.Drawing.Point(12, 91);
            this.btnInsertEquipment.Name = "btnInsertEquipment";
            this.btnInsertEquipment.Size = new System.Drawing.Size(106, 23);
            this.btnInsertEquipment.TabIndex = 0;
            this.btnInsertEquipment.Text = "Insert Equipment";
            this.btnInsertEquipment.UseVisualStyleBackColor = true;
            this.btnInsertEquipment.Click += new System.EventHandler(this.btnInsertEquipment_Click);
            // 
            // btnDefaultData
            // 
            this.btnDefaultData.Location = new System.Drawing.Point(488, 307);
            this.btnDefaultData.Name = "btnDefaultData";
            this.btnDefaultData.Size = new System.Drawing.Size(106, 23);
            this.btnDefaultData.TabIndex = 14;
            this.btnDefaultData.Text = "default data";
            this.btnDefaultData.UseVisualStyleBackColor = true;
            this.btnDefaultData.Click += new System.EventHandler(this.btnDefaultData_Click);
            // 
            // btnUpdateEquipment
            // 
            this.btnUpdateEquipment.Location = new System.Drawing.Point(12, 120);
            this.btnUpdateEquipment.Name = "btnUpdateEquipment";
            this.btnUpdateEquipment.Size = new System.Drawing.Size(106, 23);
            this.btnUpdateEquipment.TabIndex = 0;
            this.btnUpdateEquipment.Text = "Update Equipment";
            this.btnUpdateEquipment.UseVisualStyleBackColor = true;
            this.btnUpdateEquipment.Click += new System.EventHandler(this.btnUpdateEquipment_Click);
            // 
            // btnDeleteEquipment
            // 
            this.btnDeleteEquipment.Location = new System.Drawing.Point(12, 149);
            this.btnDeleteEquipment.Name = "btnDeleteEquipment";
            this.btnDeleteEquipment.Size = new System.Drawing.Size(114, 23);
            this.btnDeleteEquipment.TabIndex = 0;
            this.btnDeleteEquipment.Text = "Delete Equipment";
            this.btnDeleteEquipment.UseVisualStyleBackColor = true;
            this.btnDeleteEquipment.Click += new System.EventHandler(this.btnDeleteEquipment_Click);
            // 
            // btnSaveXML
            // 
            this.btnSaveXML.Location = new System.Drawing.Point(0, 178);
            this.btnSaveXML.Name = "btnSaveXML";
            this.btnSaveXML.Size = new System.Drawing.Size(126, 23);
            this.btnSaveXML.TabIndex = 0;
            this.btnSaveXML.Text = "Save  Equipment  XML";
            this.btnSaveXML.UseVisualStyleBackColor = true;
            this.btnSaveXML.Click += new System.EventHandler(this.btnSaveXML_Click);
            // 
            // btnLoadXML
            // 
            this.btnLoadXML.Location = new System.Drawing.Point(0, 207);
            this.btnLoadXML.Name = "btnLoadXML";
            this.btnLoadXML.Size = new System.Drawing.Size(126, 23);
            this.btnLoadXML.TabIndex = 0;
            this.btnLoadXML.Text = "Load Equipment XML";
            this.btnLoadXML.UseVisualStyleBackColor = true;
            this.btnLoadXML.Click += new System.EventHandler(this.btnLoadXML_Click);
            // 
            // btnMaintainEquipmentTypes
            // 
            this.btnMaintainEquipmentTypes.Location = new System.Drawing.Point(608, 525);
            this.btnMaintainEquipmentTypes.Name = "btnMaintainEquipmentTypes";
            this.btnMaintainEquipmentTypes.Size = new System.Drawing.Size(154, 23);
            this.btnMaintainEquipmentTypes.TabIndex = 15;
            this.btnMaintainEquipmentTypes.Text = "Maintain Equipment Types";
            this.btnMaintainEquipmentTypes.UseVisualStyleBackColor = true;
            this.btnMaintainEquipmentTypes.Click += new System.EventHandler(this.btnMaintainEquipmentTypes_Click);
            // 
            // btnLoadDB
            // 
            this.btnLoadDB.Location = new System.Drawing.Point(0, 236);
            this.btnLoadDB.Name = "btnLoadDB";
            this.btnLoadDB.Size = new System.Drawing.Size(126, 23);
            this.btnLoadDB.TabIndex = 0;
            this.btnLoadDB.Text = "Load Equipment DB";
            this.btnLoadDB.UseVisualStyleBackColor = true;
            this.btnLoadDB.Click += new System.EventHandler(this.btnLoadDB_Click);
            // 
            // frmComputerWorld
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1243, 612);
            this.Controls.Add(this.btnMaintainEquipmentTypes);
            this.Controls.Add(this.btnDefaultData);
            this.Controls.Add(this.cbxEquipmentType);
            this.Controls.Add(this.txtProcessorSpeed);
            this.Controls.Add(this.txtHddSize);
            this.Controls.Add(this.txtCost);
            this.Controls.Add(this.txtModel);
            this.Controls.Add(this.txtManufacturer);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnMakeComputers);
            this.Controls.Add(this.lbxSoftware);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.dgvEquipment);
            this.Controls.Add(this.lbxComputer);
            this.Controls.Add(this.btnLoadDB);
            this.Controls.Add(this.btnLoadXML);
            this.Controls.Add(this.btnSaveXML);
            this.Controls.Add(this.btnDeleteEquipment);
            this.Controls.Add(this.btnUpdateEquipment);
            this.Controls.Add(this.btnInsertEquipment);
            this.Controls.Add(this.btnMakeComputer);
            this.Name = "frmComputerWorld";
            this.Text = "Equipment ";
            this.Load += new System.EventHandler(this.frmComputerWorld_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEquipment)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMakeComputer;
        private System.Windows.Forms.ListBox lbxComputer;
        private System.Windows.Forms.DataGridView dgvEquipment;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.ListBox lbxSoftware;
        private System.Windows.Forms.Button btnMakeComputers;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.TextBox txtManufacturer;
        private System.Windows.Forms.TextBox txtModel;
        private System.Windows.Forms.TextBox txtCost;
        private System.Windows.Forms.TextBox txtHddSize;
        private System.Windows.Forms.TextBox txtProcessorSpeed;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cbxEquipmentType;
        private System.Windows.Forms.Button btnInsertEquipment;
        private System.Windows.Forms.Button btnDefaultData;
        private System.Windows.Forms.Button btnUpdateEquipment;
        private System.Windows.Forms.Button btnDeleteEquipment;
        private System.Windows.Forms.Button btnSaveXML;
        private System.Windows.Forms.Button btnLoadXML;
        private System.Windows.Forms.Button btnMaintainEquipmentTypes;
        private System.Windows.Forms.Button btnLoadDB;
    }
}

