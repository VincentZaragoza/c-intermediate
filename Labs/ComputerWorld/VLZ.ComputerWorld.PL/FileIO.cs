﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VLZ.ComputerWorld.PL
{
    public class FileIO
    {
        public FileIO()
        {

        }

        public FileIO(string _fileName)
        {
            fileName = _fileName;
        }

        public FileIO(string _fileName,string _path)
        {
            fileName = _fileName;
            path = _path;
        }

        private string fileName;

        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        private string path;

        public string Path
        {
            get { return path; }
            set { path = value; }
        }

        //Create a calculated field based on the path and filenam
        public string FilePath
        {
            get { return path + "\\" + fileName; }
            
        }

        public void Move(string target)
        {
            File.Move(FilePath,target);
        }

        public void Rename(string target)
        {
            File.Move(FilePath, target);
        }

        public void Copy(string target)
        {
            File.Copy(FilePath, target);
        }

        public void Delete()
        {
            if (File.Exists(FilePath))
                File.Delete(FilePath);
        }

        public string Read()
        {
            try
            {
                if (File.Exists(FilePath))
                {
                    StreamReader sr = new StreamReader(FilePath);

                    sr = File.OpenText(FilePath);
                    string contents = sr.ReadToEnd();
                    sr.Close();
                    sr = null;
                    return contents;
                }
                else
                {
                    throw new FileNotFoundException();
                }

            }
            catch (Exception ex)
            {

                throw ex; 
            }
        }

        public void Save(string contents)
        {
            try
            {
                StreamWriter sw = File.AppendText(FilePath);
                sw.WriteLine(contents);
                sw.Close();
            }
            catch (Exception ex)
            {
                throw ex; 
            }
        }
    }
}
