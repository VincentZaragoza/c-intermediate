﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace VLZ.ComputerWorld.PL
{
    public class Database
    {
        SqlConnection connection;
        string connectionString;

        public Database()
        {

        }
        public Database(string _connectionString)
        {
            connectionString = _connectionString;
        }

        private ConnectionState Open()
        {
            try
            {
                connection = new SqlConnection();
                connection.ConnectionString = connectionString;
                //Call the .Net open
                connection.Open();
                return connection.State;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public void Close()
        {
            try
            {
                connection.Close();
                connection = null;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        private int ExecuteSQL(SqlCommand cmd)
        {
            try
            {
                Open();
                if (connection.State == ConnectionState.Open)
                {
                    cmd.Connection = connection;
                    int result = cmd.ExecuteNonQuery();
                    Close();
                    return result;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw ex;
             }
        }

        public int Update(SqlCommand cmd)
        {
            return ExecuteSQL(cmd);
        }
        public int Insert(SqlCommand cmd)
        {
            return ExecuteSQL(cmd);
        }

        public int Delete(SqlCommand cmd)
        {
            return ExecuteSQL(cmd);
        }

        public DataTable Select(SqlCommand cmd)
        {
            try
            {
                DataTable dt = new DataTable();
                Open();
                if (connection.State == ConnectionState.Open)
                {
                    cmd.Connection = connection;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                    Close();
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Select(string sql)
        {
            try
            {
                DataTable dt = new DataTable();
                Open();
                if(connection.State == ConnectionState.Open)
                {
                    SqlCommand cmd = new SqlCommand(sql,connection);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);                                       
                    da.Fill(dt);
                    Close();                
                }                
                return dt;                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
