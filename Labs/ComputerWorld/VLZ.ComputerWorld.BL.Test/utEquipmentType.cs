﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VLZ.ComputerWorld.BL;

namespace VLZ.ComputerWorld.BL.Test
{
    [TestClass]
    public class utEquipmentType
    {
        [TestMethod]
        public void LoadTest()
        {
            EquipmentTypeList equipmentTypes = new EquipmentTypeList();
            equipmentTypes.Load();

            int expected = 5;
            int actual = equipmentTypes.Count;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void InsertTest()
        {
            EquipmentType equipmentType = new EquipmentType();
            equipmentType.ID = -99;
            equipmentType.Name = "Squishy";

            bool actual = equipmentType.Insert();

            Assert.IsTrue(actual);
            
        }

        [TestMethod]
        public void DeleteTest()
        {
            EquipmentType equipmentType = new EquipmentType();
            equipmentType.ID = -99;

            int actual = equipmentType.Delete();
            int expected = 1;

            Assert.AreEqual(expected, actual);
        }
    }
}
