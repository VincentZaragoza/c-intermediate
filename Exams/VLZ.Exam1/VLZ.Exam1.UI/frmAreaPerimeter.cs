﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VLZ.Exam1.UI
{
    public partial class frmAreaPerimeter : Form
    {
        private double CalculateArea(double l, double w)
        {
            double result = 0;

            result = l * w;

            return result;
        }

        private void CalculatePerimeter(double l, double w, ref double perimeter)
        {
            perimeter = (2 * w) + (2 * l);
        }

        public frmAreaPerimeter()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            double length = 0, width = 0, area = 0, perimeter = 0;


            try
            {
                if (double.TryParse(txtLength.Text, out length))
                {
                    if (double.TryParse(txtWidth.Text, out width))
                    {
                        CalculatePerimeter(length, width, ref perimeter);
                        lblPerimeter.Text = perimeter.ToString();


                        area = CalculateArea(length, width);
                        lblArea.Text = area.ToString();


                        lblStatus.Text = string.Empty;
                        lblStatus.ForeColor = Color.Blue;
                    }
                    else
                    {
                        lblStatus.Text = "Please enter a number";
                        lblStatus.ForeColor = Color.Red;
                        txtWidth.Focus();
                        txtWidth.SelectAll();
                    }
                }
                else
                {
                    lblStatus.Text = "Please enter a number";
                    lblStatus.ForeColor = Color.Red;
                    txtLength.Focus();
                    txtLength.SelectAll();
                }
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message;
                lblStatus.ForeColor = Color.Red;

                txtWidth.Text = string.Empty;
                txtLength.Focus();
                txtLength.SelectAll();
            }
        }

        private void txtLength_TextChanged(object sender, EventArgs e)
        {
            lblArea.Text = string.Empty;
            lblPerimeter.Text = string.Empty;
        }

        private void txtWidth_TextChanged(object sender, EventArgs e)
        {
            lblArea.Text = string.Empty;
            lblPerimeter.Text = string.Empty;
        }
    }
}
